COMPILER = gcc

SRCDIR = codes
HDDIR = headers
OBJDIR = executables

CFLAGS = -g -lm -std=c99 -fstack-protector-all -pedantic -I$(HDDIR)


# Colocar aqui os arquivos que devem ser compilados
SOURCES := $(wildcard $(SRCDIR)/*.c $(SRCDIR)/**/*.c )
HEADERS := $(wildcard $(HDDIR)/*.h $(HDDIR)/**/*.h )
OBJECTS := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)


siguel: $(OBJECTS)
	$(COMPILER) -o $@ $^ $(CFLAGS)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(COMPILER) -c -o $@ $< $(CFLAGS)

debug: $(OBJECTS)
	$(COMPILER) -o siguel $^ $(CFLAGS) -g

.PHONY: clean
clean:
	rm $(OBJECTS)