#ifndef INCLUDES
#define INCLUDES


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdarg.h>
#include <stdbool.h>
#include <limits.h>
#include <float.h>
#include "struct.h"
#include "parametros.h"
#include "comandos.h"
#include "figuras.h"
#include "./generics/arvore.h"
#include "./generics/hashTable.h"
#include "./generics/lista.h"
#include "./generics/uteis.h"
#include "./generics/arquivos.h"
#include "./generics/closest_pair.h"

#endif