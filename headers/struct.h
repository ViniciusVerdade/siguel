#ifndef ESTRUTURA
#define ESTRUTURA

#include "includes.h"

enum{
    QUADRA,
    HIDRANTE,
    SEMAFORO,
    TORRE_RADIO,
    FIGS,
    FIGS_EXTRA,
    EQ_URBANO,
    RETANGULO,
    CIRCULO

};

struct figur{
    char* id;
    float radius;
    float width;
    float height;
    float x;
    float y;
    float xcenter;
    float ycenter;
    char *perColor; /*cor da borda*/
    char *fillColor; /*cor do preenchimento*/
};
typedef struct figur Figure;

struct arg{
    char* path; // -e
    char* directory;// -o
    char* pathOfNameGeoFile;// soma do path + directory + fileName
    char* pathOfNameQryFile;// soma do path + directory + fileName
    char* pathOfNamePmFile;// soma do path + directory + fileName
    char* pathOfNameEcFile;// soma do path + directory + fileName
    char* NameGeoFile; // -f
    char* NameQryFile;// -q
    char* NamePmFile;
    char* NameEcFile;
    char* NameTxtxtFile;
    char* NameSvgFile;
    FILE *mainFile; //tirar para abrir direto na main
    
    FILE *geoFile;
    FILE *qryFile;
    FILE *ecFile;
    FILE *pmFile;
    FILE *svgFile;
    FILE *txtFile;
    
    int parada;
};
typedef struct arg Arguments;

struct col{
    char* corFill;
    char* corBorder;
};
typedef struct col Colors;

typedef struct HashTables{
    HashTable TABELA_QUADRA;
    HashTable TABELA_HIDRANTE;
    HashTable TABELA_SEMAFORO;
    HashTable TABELA_TORRE_RADIO;
    HashTable TABELA_PESSOAS;
    HashTable TABELA_COMERCIO;
}Tabelas;

Colors *newColors();

Arguments *newArguments();
Figure *newFigure();
void freeFigures(Figure **fig);

void freeArguments(Arguments **arg);

#endif