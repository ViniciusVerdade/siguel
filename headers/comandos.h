#ifndef COMANDOS
#define COMANDOS


#include "includes.h"
#include "./generics/lista.h"

enum{
    filler,
    NADA,
    SIM,
    NAO
};

void comando(char* inst, int caract, Lista* VETOR_LISTAS,Kdtree ELEMENTOS,Arguments* arg,Colors **cores,int tipoArq);

int receberComando(char *inst,int tipoArq);

void criarEquipamentoUrbano(Lista EQ_ESCOLHIDO,Kdtree ELEMENTOS,int tipo, char* inst, Colors* col);

void alterarCorEU(int tipo, char* inst,Colors** col);

void reportarFiguras(Lista* VETOR_LISTAS,int tipo ,char* inst,Arguments* arg);

void removeObjeto(Lista* VETOR_LISTAS,int tipo,char* inst,Arguments* arg);

void mudarCor(Lista* VETOR_LISTAS, char* inst,Colors** col);

void procurarObjeto(Lista* VETOR_LISTAS, char* inst,Arguments* arg);

void teste_sobreposicao(Lista* FIGURAS_LISTA,char* inst,Arguments* arg);

int checando_sobreposicao(void* figura1,void* figura2,char* inst);

void sobrepoe_borda(Lista* FIGURAS_LISTA,void* figura1,void* figura2); 

void checar_ponto(Lista FIGURAS_LISTA,char* inst,Arguments* arg);

void distancia_figuras(Lista FIGURAS_LISTA,char* inst,Arguments* arg);

void distancia_comparada_figuras(Lista FIGURAS_LISTA,char* inst,Arguments* arg);

#endif 