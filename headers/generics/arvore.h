#ifndef KDTREE
#define KDTREE

#include "../headers/includes.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
enum{
    X,
    Y
};
typedef void *Elemento;

typedef void *Kdtree;

Kdtree create_arvore(_Bool(*checarIgual)(Item a,Item b),int (*which_sideX)(Item a, Item b),int (*which_sideY)(Item a, Item b) );

Kdtree insert_arvore(Kdtree kdtree, Item item);

void set_item_arvore(Kdtree _tree,Item item);
//bool are_values_equal(Item item1,Item item2);

Kdtree search_arvore(Kdtree tree, Item item);

Item find_min(Kdtree tree, int d);

int min_between_3(int x, int y, int z);

Kdtree min_kdtree(Kdtree _a,Kdtree _b,Kdtree _c, int dim);

// void copyPoint(Item item1, Item item2);

Kdtree delete_node_recursive(Kdtree tree, Item item, int depth);

Kdtree delete_node(Kdtree tree,Item item);

Item get_item_arvore(Kdtree _tree);

Kdtree find_max(Kdtree tree, int d);

Kdtree max_kdtree(Kdtree _a,Kdtree _b,Kdtree _c, int depth);

void print_inside(Kdtree _tree, void (*print)(void* item));

Lista convert_tree_to_list(Kdtree _tree);

void percorrer_tree(Kdtree _tree,void (*function_during_tree)(Item a,Item b,va_list parameters_list),va_list parameters);
#endif 