#ifndef HASH_TABLE
#define HASH_TABLE

#include "../headers/includes.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef void* HashTable;

//* ONDE POSSUI O ELEMENTO DA TABELA

struct HashInfo{
    Item value;
    char* key;
};
//typedef void* HashInfo;
typedef struct HashInfo HashInfo;

HashTable create_table(int n);

int hash_key(char* key,int size,int initi_val);

void insert_table(HashTable tabela, HashInfo info);

_Bool if_exist_key(HashTable tabela, char* key);

//RETORNA A POSIÇÃO DA TABELA EM QUE SE ENCONTRA O ELEMENTO COM A KEY ENVIADA
int get_info_table(HashTable tabela, char* key);// Talvez tenha que voltar a retornar hashinfo

HashInfo remove_key(HashTable tabela, char* key);

#endif