#ifndef UTEIS
#define UTEIS

//conta quantos caracteres tem em uma linha de arquivo
int caractLinha( FILE *fin );
//pula linha ao chegar em seu final
void pularLinha(FILE* var);
//insere num vetor aux a linha de um arquivo, alocando dinamicamente "caract" bits de memoria
void getLine(char* aux,FILE* file,int caract);

char* alocarMemoria(int caract);

float max(float a, float b) ;

float min(float a, float b) ;
//engole o caracter de um char* à esquerda
void shiftLeft(char* text, int n) ;
//dados dois pontos (x,y) retorna a distancia entre eles
float dotDistance(float x1,float y1,float x2,float y2);
//realloc que funciona
void reallocMem();

char* cut_until_char(char* palavra);

// char* get_palavra_until_char(char* palavra, char esc,char* aux2);

char* separate_char(char* aux,char*aux2);

#endif