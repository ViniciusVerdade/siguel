#ifndef GRAPH
#define GRAPH

#include "../headers/includes.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef void* Grafo;

typedef void* Vertice;

struct Grafo{
    Lista vertices;
    Lista arestas;
    int qtdVertices;
};

struct Vertice{
    char* index;
    Item valor;
    Lista arestas;
};

/**
* * Necessita de uma quantidade de vértices;
* * Retorna um grafo vazio.
*/
Grafo create_grafo(int nVertices);

/**
 * * Necessita de um grafo e dois vértices, que estejam no grafo, para poder criar uma aresta que relacione os dois vértices;
 * * Retorna a aresta criada.
 * */
Aresta insert_aresta(Grafo grafo,Vertice v1,Vertice v2);

/**
 * * Recebe um grafo e um par de vértices que estão dentro de grafo e um valor a ser atribuido;
 * * Retorna a aresta em questão com o valor inserido nela.
 * */
Aresta set_item_aresta(Grafo grafo,Vertice v1,Vertice v2,Item valor);

/**
 * * Recebe um grafo e um par de vértices que estão dentro de grafo;
 * * Retorna o item atribuido a aresta de vertices v1 e v2.
 * */
Item get_item_aresta(Grafo grafo,Vertice v1,Vertice v2);

/**
 * * Recebe um grafo e um par de vértices que estão dentro de grafo;
 * * Retorna a aresta de vertices v1 e v2 que fora removida do grafo;
 * */
Aresta remove_aresta(Grafo grafo,Vertice v1,Vertice v2);

/**
 * * Recebe um grafo e um par de vértices que estão dentro de grafo;
 * * Retorna aresta caso v1 e v2 formem uma aresta, caso contrário, o algoritmo retornará nulo.
 * */
Aresta are_vertices_adjacentes(Grafo grafo,Vertice v1,Vertice v2);

/**
 * * Recebe um grafo e um vértice que está dentro do grafo;
 * * Retorna uma lista de vertices que possuem arestas com o vertice v.
 * */
Lista vertices_adjacentes(Grafo grafo,Vertice v);

/**
 * * Recebe um grafo e um vértice que esta dentro de grafo;
 * * Retorna o vertice em questão com o item atribuido nele.
 * */
Vertice set_item_vertice(Grafo grafo,Vertice v,Item valor);

/**
 * * Recebe um grafo e um vértice que esta dentro de grafo;
 * * Retorna o item atribuido ao vertice v.
 * */
Item set_item_vertice(Grafo grafo,Vertice v);

/**
 * * Recebe um grafo e um par de vértices que esta dentro de grafo;
 * * Retorna uma lista com os vértices que formam a menor distancia.
 * */
Lista closest_path(Grafo grafo,Vertice v1,Vertice v2);

#endif 