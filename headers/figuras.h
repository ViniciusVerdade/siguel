#ifndef FIGURAS
#define FIGURAS

#include "includes.h"
#include "./generics/lista.h"

typedef void* Quadra;
typedef void* Hidrante;
typedef void* Semaforo;
typedef void* Radio;

struct Elemento{
    char* tipo;
    char* id;
    float x,y;
    char* perColor,*fillColor;
    float width;
    float height;
};

struct Quadra{
    char* tipo;
    char* id;
    float x;
    float y;
    char *perColor; 
    char *fillColor;
    float width;
    float height;
    float xcenter;
    float ycenter;
};

struct Hidrante{
    char* tipo;
    char* id;
    float x;
    float y;
    char *perColor; 
    char *fillColor;
    float width;
    float height;
} ;

struct Semaforo{
    char* tipo;
    char* id;
    float x;
    float y;
    char *perColor; 
    char *fillColor;
    float width;
    float height;
};

struct Radio{
    char* tipo;
    char* id;
    float x;
    float y;
    char *perColor; 
    char *fillColor;
    float width;
    float height;
} ;

Quadra newQuadra();
Hidrante newHidrante();
Semaforo newSemaforo();
Radio newRadio();

void localizarArea(Lista* VETOR_LISTAS,char* inst,int tipo,int tipo2,Arguments* arg);

void informar_presenca(Lista* VETOR_LISTAS,void* OBJ_PRESENTES,char* equipamento);

void reportar_figuras(Lista OBJ_PRESENTES,Arguments *arg);

void remover_figuras(Lista* VETOR_LISTAS,Lista OBJ_PRESENTES,Arguments *arg);

void criar_figura(Lista*VETOR_LISTAS,char* inst,int tipo);

void insert_info_fig(Figure* figura,char* aux,char* aux2,int tipo);

#endif