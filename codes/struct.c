#include "../headers/includes.h"
#include "enum.h"

Colors *newColors()
{
    Colors *cor = (Colors*)malloc(sizeof(Colors));

    cor->corBorder = NULL;
    cor->corFill = NULL;
    return cor;
}

Arguments *newArguments()
{
  Arguments *arg = (Arguments*)malloc(sizeof(Arguments));
  
  arg->path = NULL;
  arg->directory = NULL;
  arg->pathOfNameGeoFile = NULL;
  arg->pathOfNameQryFile = NULL;
  arg->NameGeoFile = NULL;
  arg->NameQryFile = NULL;
  arg->NameSvgFile = NULL;
  arg->NameTxtxtFile = NULL;
  arg->mainFile = NULL;
  arg->svgFile = NULL;
  arg->txtFile = NULL;
  arg->parada = 0;
  return arg;
}

void freeArguments(Arguments **arg)
{
  free((*arg)->path);
  free((*arg)->directory);
  free((*arg)->pathOfNameGeoFile);
  free((*arg)->pathOfNameQryFile);
  free((*arg)->mainFile);
  free((*arg)->NameGeoFile); 
  free((*arg)->NameQryFile); 
  free((*arg)->NameSvgFile); 
  free((*arg)->NameTxtxtFile); 
  free((*arg)->mainFile); 
  free((*arg)->svgFile); 
  free((*arg)->txtFile); 

  free(*arg);
  *arg = NULL;
}

Figure *newFigure()
{
    Figure *fig = (Figure*)malloc(sizeof(Figure)); 

    fig->id = NULL;

    fig->radius = 0;
    fig->width = 0;
    fig->height = 0;

    fig->x = 0;
    fig->y = 0;

    fig->perColor = NULL; /*cor da borda*/
    fig->fillColor = NULL; /*cor do preenchimento*/

    fig->xcenter = 0;
    fig->ycenter = 0;

    return fig;
}

void freeFigures(Figure **fig)
{
  free((*fig)->id);
  free((*fig)->perColor);
  free((*fig)->fillColor);

  free(*fig);
  *fig = NULL;
}