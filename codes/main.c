#include "../headers/includes.h"
#include "enum.h"
void printelemento(void* a){
    struct Elemento* A = a;
    printf("%.0f ",A->x);
    printf("%.0f ",A->y);
    printf("%s\n", A->fillColor);
}

_Bool compare_values(Elemento a, Elemento b){
    struct Elemento* A = a;
    struct Elemento* B = b;

    if( strcmp(A->id,B->id) == 0 ){
        return true;
    }
    return false;
}
//A é o elemento que eu quero inserir, B é nodo atual
int choose_sideX(Elemento a,Elemento b){
    struct Elemento* A = a;
    struct Elemento* B = b;
    
    if(A->x > B->x){
        return 1;
    }
    return -1;
}
//A é o elemento que eu quero inserir, B é nodo atual
int choose_sideY(Elemento a,Elemento b){
    struct Elemento* A = a;
    struct Elemento* B = b;

    if(A->y > B->y){
        return 1;
    }
    return -1;
}

int main(int argc, const char* argv[])
{
    Arguments *arg = argumentos(argc,argv); // struct que possui os parametros passados pela linha de comando
    FILE *mainFile;
    Lista VETOR_LISTAS[6];


    Kdtree ELEMENTOS;
    ELEMENTOS = create_arvore(compare_values, choose_sideX, choose_sideY);
    
    /** KDTREE
     * ? MEXENDO COM A KDTREE!
     *struct Quadra* quad = newQuadra();
     *quad->id = alocarMemoria(10);
     *strcpy(quad->id, "feijao");
     *quad->x = 305;
     *quad->y = 100;
     *quad->height = 50;
     *quad->width = 100;
     *quad->fillColor = alocarMemoria(5);
     *quad->perColor = alocarMemoria(7);
     *strcpy(quad->fillColor, "pink");
     *strcpy(quad->perColor, "black");
     *Auxiliar = insert_arvore(ELEMENTOS, (Item) quad);
     *struct Quadra *auxx = (struct Quadra*)get_item_arvore(Auxiliar);
    */

    /** HASHTABLE 
     *? MEXENDO COM HASHTABLE! 
     * HashTable TABLE = create_table(20); 
     * *INFO NÃO É UM PONTEIRO
     *HashInfo INFO = {
     *    .key = quada->id,
     *    .value = quada
     *};
     *HashInfo INFO2 = {
     *    .key = auxx->id,
     *    .value = auxx
 
     insert_table(TABLE,INFO);
     insert_table(TABLE,INFO);
     insert_table(TABLE,INFO2);
     insert_table(TABLE,INFO2);
 
     Lista LIST = convert_tree_to_list(ELEMENTOS);
     print_lista(LIST,printelemento);
    *};*/

    /** LISTA LIGADA
     * ? LISTA LIGADA
     *NÃO TENHO REFERENCIAS A NEWQUADRA, APENAS INICIOooooooooooooooo
     *insert_lista(VETOR_LISTAS[ QUADRA ], (Item) newQuadra());
     *
     *Posic p = get_first_lista(VETOR_LISTAS[QUADRA]);
     *
     *Quadra *quad = (Quadra*)get_lista(VETOR_LISTAS[QUADRA], p);
     *quad->x = 30;
     *
     *insert_lista(VETOR_LISTAS[ QUADRA ], (Item) newQuadra());
     *p = get_next_lista(VETOR_LISTAS[QUADRA],p);
     *quad = get_lista(VETOR_LISTAS[QUADRA], p);
     *quad->x = 50;
    */
    
    // VETOR_LISTAS[ QUADRA ] = create_lista();
    // VETOR_LISTAS[ HIDRANTE ] = create_lista();
    // VETOR_LISTAS[ SEMAFORO ] = create_lista();
    VETOR_LISTAS[ TORRE_RADIO ] = create_lista();
    VETOR_LISTAS[ FIGS ] = create_lista();
    VETOR_LISTAS[ FIGS_EXTRA ] = create_lista();

    
    abrindoArquivo(&arg->geoFile,arg->pathOfNameGeoFile,0);
    abrindoArquivo(&arg->svgFile,arg->NameSvgFile,1);
    abrindoArquivo(&arg->txtFile,arg->NameTxtxtFile,1);
    lendoArquivo(arg,VETOR_LISTAS,ELEMENTOS,GEO_FILE);

    fclose(arg->geoFile);
    draw_svg(VETOR_LISTAS,arg,SIM);
    fclose(arg->svgFile);
    fclose(arg->txtFile);

    if(arg->pathOfNameQryFile != NULL)
    {
        alter_nome_arquivo(SVG,arg);
        abrindoArquivo(&arg->mainFile,arg->pathOfNameQryFile,0);
        abrindoArquivo(&arg->txtFile,arg->NameTxtxtFile,2);
        abrindoArquivo(&arg->svgFile,arg->NameSvgFile,1);

        lendoArquivo(arg,VETOR_LISTAS,ELEMENTOS,QRY_FILE);
        fclose(arg->mainFile);
        draw_svg(VETOR_LISTAS,arg,NAO);
        fclose(arg->svgFile);
        fclose(arg->txtFile);

    }
    
    destruir_lista(VETOR_LISTAS[QUADRA], NULL);
    destruir_lista(VETOR_LISTAS[HIDRANTE], NULL);
    destruir_lista(VETOR_LISTAS[SEMAFORO], NULL);
    destruir_lista(VETOR_LISTAS[TORRE_RADIO], NULL);
    destruir_lista(VETOR_LISTAS[FIGS], NULL);
    destruir_lista(VETOR_LISTAS[FIGS_EXTRA], NULL);
    
    printf("CORRETO!\n");
    return 0;
}
