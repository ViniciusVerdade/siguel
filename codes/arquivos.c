#include "../headers/includes.h"

#define RAIO_EQ 5

#include "enum.h"

void abrindoArquivo(FILE** file,char* nomeARQ, int tipo){
    if(tipo == 0)
    {
        *file = fopen(nomeARQ,"r+"); /*abrindo para ler*/
        if(*file == NULL){
            printf("Erro, nao foi possivel abrir o arquivo\n");
        }
    }else if(tipo == 1)
    {
        *file = fopen(nomeARQ,"w+"); /*abrindo para escrever*/
        if(*file == NULL){
            printf("Erro, nao foi possivel abrir o arquivo\n");
        }
    }else if(tipo == 2)
    {
        *file = fopen(nomeARQ,"a"); /*abrindo para continuar a escrever*/
        if(*file == NULL){
            printf("Erro, nao foi possivel abrir o arquivo\n");
        }
    }
}
void lendoArquivo(Arguments *arg,Lista *VETOR_LISTAS,Kdtree ELEMENTOS,int arqLido){
    char *instruction;
    Colors **cores = (Colors**)calloc(4,sizeof(Colors*));
    int caract,stop = 0; //Numero de caracteres em uma linha
    arg->parada = 0;
    
    switch (arqLido)
    {
        case GEO_FILE:
            while(!feof(arg->geoFile))
            {
                caract = caractLinha(arg->geoFile);
                instruction = alocarMemoria(caract);
                getLine(instruction,arg->geoFile,caract);

                comando(instruction, caract,VETOR_LISTAS,ELEMENTOS,arg,cores,GEO_FILE);
            
                pularLinha(arg->geoFile);
                stop = parando_arquivo(arg,instruction);
                if( stop == 1)
                {
                    break;
                }
                free(instruction);
            }
            break;
        case EC_FILE:
            while(!feof(arg->ecFile))
            {
                caract = caractLinha(arg->ecFile);
                instruction = alocarMemoria(caract);
                getLine(instruction,arg->ecFile,caract);

                comando(instruction, caract,VETOR_LISTAS,arg,cores,EC_FILE);
            
                pularLinha(arg->ecFile);
                stop = parando_arquivo(arg,instruction);
                if( stop == 1)
                {
                    break;
                }
                free(instruction);
            }
        break;
        case PM_FILE:
            while(!feof(arg->pmFile))
            {
                caract = caractLinha(arg->pmFile);
                instruction = alocarMemoria(caract);
                getLine(instruction,arg->pmFile,caract);

                comando(instruction, caract,VETOR_LISTAS,arg,cores,PM_FILE);
            
                pularLinha(arg->pmFile);
                stop = parando_arquivo(arg,instruction);
                if( stop == 1)
                {
                    break;
                }
                free(instruction);
            }
        break;
        case QRY_FILE:
            while(!feof(arg->qryFile))
            {
                caract = caractLinha(arg->qryFile);
                instruction = alocarMemoria(caract);
                getLine(instruction,arg->qryFile,caract);

                comando(instruction, caract,VETOR_LISTAS,arg,cores,QRY_FILE);
            
                pularLinha(arg->qryFile);
                stop = parando_arquivo(arg,instruction);
                if( stop == 1)
                {
                    break;
                }
                free(instruction);
            }
        break;

        default:
            break;
    }

}

void nome_arquivo(int tipo,Arguments* arg)
{
    int j;
    char* aux;
    switch(tipo)
    {
        case TXT:
            arg->NameTxtxtFile = (char*)calloc(strlen(arg->NameGeoFile)+strlen(arg->directory)+1,sizeof(char));
            strcpy(arg->NameTxtxtFile,arg->directory);
            strcat(arg->NameTxtxtFile,arg->NameGeoFile);
            j = strlen(arg->NameTxtxtFile)-1;
            arg->NameTxtxtFile[j] = 't';
            arg->NameTxtxtFile[j-1] = 'x';
            arg->NameTxtxtFile[j-2] = 't';

        break;
        case SVG:
            if(arg->NameQryFile != NULL)
            {
                arg->NameSvgFile = (char*)calloc(strlen(arg->NameQryFile)+strlen(arg->NameGeoFile)+strlen(arg->directory)+1,sizeof(char));
            }
            else
            {
                arg->NameSvgFile = (char*)calloc(strlen(arg->NameGeoFile)+strlen(arg->directory)+1,sizeof(char));
            }
            strcpy(arg->NameSvgFile,arg->directory);
            strcat(arg->NameSvgFile,arg->NameGeoFile);
            j = strlen(arg->NameTxtxtFile)-1;
            arg->NameSvgFile[j] = 'g';
            arg->NameSvgFile[j-1] = 'v';
            arg->NameSvgFile[j-2] = 's';
        break;
    }
}

void alter_nome_arquivo(int tipo,Arguments* arg)
{
    int j,i,n=0;
    char* aux=(char*)calloc(strlen(arg->NameQryFile)+strlen(arg->NameGeoFile)+strlen(arg->directory)+1,sizeof(char));;
    char* nomeG=(char*)calloc(strlen(arg->NameGeoFile)+1,sizeof(char)) ; 
    strcpy(nomeG,arg->NameGeoFile);
    char* nomeS=(char*)calloc(strlen(arg->NameQryFile)+1,sizeof(char)) ; 
    strcpy(nomeS,arg->NameQryFile);

    strcpy(aux,arg->NameSvgFile);
    free(arg->NameSvgFile);
    switch(tipo)
    {
        case TXT:
        break;
        case SVG:
            arg->NameSvgFile = (char*)calloc(strlen(arg->NameQryFile)+strlen(arg->NameGeoFile)+strlen(arg->directory)+1,sizeof(char));
            strcpy(arg->NameSvgFile,arg->directory);
            

            i = strlen(nomeG)-1;
            j = i-4;
            while(j!=0)
            {
                if(nomeG[j]=='/')
                {
                    shiftLeft(nomeG,j+1);
                    break;
                }
                j--;
            }
            i = strlen(nomeG)-1;
            j=i-4;
            nomeG[j+1] = 0;
            nomeG[j+2] = 0;
            nomeG[j+3] = 0;

            i = strlen(nomeS)-1;
            j = i-4;
            while(j!=0)
            {
                if(nomeS[j]=='/')
                {
                    shiftLeft(nomeS,j+1);
                    break;
                }
                j--;
            }
            i = strlen(nomeS)-1;
            j=i-4;
            nomeS[j+1] = 0;
            nomeS[j+2] = 0;
            nomeS[j+3] = 0;

            sprintf(aux, "%s-%s.svg", nomeG, nomeS);
            strcat(arg->NameSvgFile,aux);
        break;
    }
}

void draw_svg(Lista* VETOR_LISTAS,Arguments* arg,int tipo)
{
    openingSVG(arg->svgFile);
    int i;
    Posic p;
    void* objeto;
    if(tipo == SIM)
    {
        for(i=0;i<=FIGS_EXTRA;i++)
        {
            p = get_first_lista(VETOR_LISTAS[ i ]);
            while(p)
            {
                objeto = get_lista(VETOR_LISTAS[ i ], p);
                switch(i)
                {
                    case QUADRA:
                        retangulo_quadra(arg->svgFile,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y,((struct Quadra*)objeto)->height,((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->id,((struct Quadra*)objeto)->fillColor,((struct Quadra*)objeto)->perColor);
                    break;
                    case HIDRANTE:
                        circulo_figura(arg->svgFile,((struct Hidrante*)objeto)->x,((struct Hidrante*)objeto)->y,RAIO_EQ,((struct Hidrante*)objeto)->fillColor,((struct Hidrante*)objeto)->perColor);
                    break;
                    case SEMAFORO:
                        circulo_figura(arg->svgFile,((struct Semaforo*)objeto)->x,((struct Semaforo*)objeto)->y,RAIO_EQ,((struct Semaforo*)objeto)->fillColor,((struct Semaforo*)objeto)->perColor);
                    break;
                    case TORRE_RADIO:
                        circulo_figura(arg->svgFile,((struct Radio*)objeto)->x,((struct Radio*)objeto)->y,RAIO_EQ,((struct Radio*)objeto)->fillColor,((struct Radio*)objeto)->perColor);
                    break;
                    case FIGS:
                        if( ((Figure*)objeto)->radius == 0 )
                        {
                            retangulo_figura(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->height,((Figure*)objeto)->width,((Figure*)objeto)->fillColor,((Figure*)objeto)->perColor);
                        }
                        else
                        {
                            circulo_figura(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->radius,((Figure*)objeto)->fillColor,((Figure*)objeto)->perColor);
                        }
                    break;
                    case FIGS_EXTRA:
                        if( ((Figure*)objeto)->radius==0 )
                        {
                            retangulo_pontilhado(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->height,((Figure*)objeto)->width,((Figure*)objeto)->id);
                        }
                        else
                        {  
                            circulo_pontilhado(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->radius);
                        }
                    break;
                }
                p = get_next_lista(VETOR_LISTAS[i],p);
            }
        }
    }
    else
    {
        for(i=0;i<=FIGS_EXTRA;i++)
        {
            p = get_first_lista(VETOR_LISTAS[ i ]);
            if(i == FIGS)
            {
                continue;
            }
            while(p)
            {
                objeto = get_lista(VETOR_LISTAS[ i ], p);
                switch(i)
                {
                    case QUADRA:
                        retangulo_quadra(arg->svgFile,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y,((struct Quadra*)objeto)->height,((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->id,((struct Quadra*)objeto)->fillColor,((struct Quadra*)objeto)->perColor);
                    break;
                    case HIDRANTE:
                        circulo_figura(arg->svgFile,((struct Hidrante*)objeto)->x,((struct Hidrante*)objeto)->y,RAIO_EQ,((struct Hidrante*)objeto)->fillColor,((struct Hidrante*)objeto)->perColor);
                    break;
                    case SEMAFORO:
                        circulo_figura(arg->svgFile,((struct Semaforo*)objeto)->x,((struct Semaforo*)objeto)->y,RAIO_EQ,((struct Semaforo*)objeto)->fillColor,((struct Semaforo*)objeto)->perColor);
                    break;
                    case TORRE_RADIO:
                        circulo_figura(arg->svgFile,((struct Radio*)objeto)->x,((struct Radio*)objeto)->y,RAIO_EQ,((struct Radio*)objeto)->fillColor,((struct Radio*)objeto)->perColor);
                    break;
                    case FIGS_EXTRA:
                        if( ((Figure*)objeto)->radius==0 )
                        {
                            if(strcmp( ((Figure*)objeto)->id,"SOBREPOE") == 0 )
                            {
                                continue;
                            }
                            else
                            {
                                retangulo_pontilhado(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->height,((Figure*)objeto)->width,((Figure*)objeto)->id);
                            }
                        }
                        else
                        {  
                            if(strcmp( ((Figure*)objeto)->id,"SOBREPOE") == 0 )
                            {
                                continue;
                            }
                            else
                            {
                                circulo_pontilhado(arg->svgFile,((Figure*)objeto)->x,((Figure*)objeto)->y,((Figure*)objeto)->radius);
                            }
                        }
                    break;
                }
                p = get_next_lista(VETOR_LISTAS[i],p);
            }

        }
    }
    closingSVG(arg->svgFile);
}
void retangulo_pontilhado(FILE *file,float x,float y,float height,float width,char* id)
{   
    fprintf(file,"%s%.1f%s%.1f%s%.1f%s%.1f%s","<rect x=\"",x,"\" y=\"",y,"\" width=\"",width,"\" height=\"",height,"\" stroke-dasharray=\"1, 1\" style=\"fill:transparent;stroke-width:1;stroke:purple;\" />\n");
    if(strcmp(id,"SOBREPOE")==0)
    {
        fprintf(file,"%s%.1f%s%.1f%s","<text x=\"",x,"\" y=\"",y,"\" style=\"fill:purple;font-size:12.0px;font-family:sans-serif\">Sobrepõe</text>\n");
    }
}
void retangulo_figura(FILE *file,float x,float y,float height,float width,char* fillColor,char* perColor)
{
    fprintf(file,"%s%.1f%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<rect x=\"",x,"\" y=\"",y,"\" width=\"",width,"\" height=\"",height,"\" style=\"fill:",fillColor,";stroke-width:1;stroke:",perColor,";opacity:0.4;\" />\n"); /*printando um retangulo*/
}
void retangulo_quadra(FILE *file,float x,float y,float height,float width,char* id,char* fillColor,char* perColor)
{
    fprintf(file,"%s%.1f%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<rect x=\"",x,"\" y=\"",y,"\" width=\"",width,"\" height=\"",height,"\" style=\"fill:",fillColor,";stroke-width:1;stroke:",perColor,";opacity:0.4;\" />\n"); /*printando um retangulo*/
    fprintf(file,"<text x=\"%.1f\" y=\"%.1f\" style=\"fill:black;font-size:12.0px;font-family:sans-serif\">%s</text>\n",x + 5,(y+height)-5,id);
}
void circulo_pontilhado(FILE *file,float x,float y,float radius)
{
    fprintf(file,"%s%.1f%s%.1f%s%.1f%s","<circle cx=\"",x,"\" cy=\"",y,"\" r=\"",radius,"\" stroke-dasharray=\"1, 1\" style=\"fill:transparent;stroke-width:1;stroke:purple;\" />\n");
}
void circulo_figura(FILE *file,float x,float y,float radius,char* fillColor,char* perColor)
{
    fprintf(file,"%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<circle cx=\"",x,"\" cy=\"",y,"\" r=\"",radius,"\" style=\"fill:",fillColor,";stroke-width:1;stroke:",perColor,";opacity:0.4;\" />\n");
}

void closingSVG(FILE *arqsvg)
{
    fprintf(arqsvg,"%s","</svg>");
}
void openingSVG(FILE* file)
{
    fprintf(file,"%s","<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"10000\" height=\"10000\">\n");/*Abrindo a imagem svg*/
}

int parando_arquivo(Arguments* arg,char* inst)
{
    if( strcmp(inst,"")==0)
    {
        return 1;
    }
    if(arg->parada == 1)
    {
        return 1;
    }
    return 0;
}