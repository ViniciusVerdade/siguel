#include "../headers/includes.h"

#include "enum.h"


void comando(char* inst, int caract, Lista* VETOR_LISTAS,Kdtree ELEMENTOS,Arguments* arg,Colors **cores,int tipoArq)
{
    int comando;
    comando = receberComando(inst,tipoArq);
    switch(comando)
    {
        //? DONE! (agora checar se está funcionando com tree)
        case CRIA_QUADRA:
            criarEquipamentoUrbano(NULL,ELEMENTOS,CRIA_QUADRA,inst,cores[QUADRA]);
        break;
        //? DONE! (agora checar se está funcionando com tree)
        case CRIA_HIDRA:
            criarEquipamentoUrbano(NULL,ELEMENTOS,CRIA_HIDRA,inst,cores[HIDRANTE]);
        break;
        //? DONE! (agora checar se está funcionando com tree)
        case CRIA_SEMA:
            criarEquipamentoUrbano(NULL,ELEMENTOS,CRIA_SEMA,inst,cores[SEMAFORO]);
        break;
        //? DONE! (agora checar se está funcionando com tree)
        case CRIA_RADIO:
            criarEquipamentoUrbano(VETOR_LISTAS[ TORRE_RADIO ],ELEMENTOS,CRIA_RADIO,inst,cores[TORRE_RADIO]);
        break;
        //* SEM NECESSIDADE DE ALTERAÇÃO
        case COR_QUADRA:
            alterarCorEU(QUADRA,inst,cores);
        break;
        //* SEM NECESSIDADE DE ALTERAÇÃO
        case COR_HIDRA:
            alterarCorEU(HIDRANTE,inst,cores);
        break;
        //* SEM NECESSIDADE DE ALTERAÇÃO
        case COR_SEMA:
            alterarCorEU(SEMAFORO,inst,cores);
        break;
        //* SEM NECESSIDADE DE ALTERAÇÃO
        case COR_RADIO:
            alterarCorEU(TORRE_RADIO,inst,cores);
        break;
        case REPORTA_RET:
            reportarFiguras(VETOR_LISTAS,REPORTA_RET,inst,arg);
        break;
        case REPORTA_CIRC:
            reportarFiguras(VETOR_LISTAS,REPORTA_CIRC,inst,arg);
        break;
        case REMOVE_QUADRA_RET:
            removeObjeto(VETOR_LISTAS,REMOVE_QUADRA_RET,inst,arg);
        break;
        case REMOVE_EQUIP_RET:
            removeObjeto(VETOR_LISTAS,REMOVE_EQUIP_RET,inst,arg);
        break;
        case REMOVE_QUADRA_CIRC:
            removeObjeto(VETOR_LISTAS,REMOVE_QUADRA_CIRC,inst,arg);
        break;
        case REMOVE_EQUIP_CIRC:
            removeObjeto(VETOR_LISTAS,REMOVE_EQUIP_CIRC,inst,arg);
        break;
        case MUDAR_COR:
            mudarCor(VETOR_LISTAS,inst,cores);
        break;
        case INFO_TUDO:
            procurarObjeto(VETOR_LISTAS,inst,arg);
        break;
        case MENOR_DIST:
            closest_pair_lista(VETOR_LISTAS,arg);
        break;
        case ALTERAR_MAX:
            printf(" ");
        break;
        case CRIA_CIRCULO:
            criar_figura(VETOR_LISTAS,inst,CIRCULO);
        break;
        case CRIA_RETANGULO:
            criar_figura(VETOR_LISTAS,inst,RETANGULO);
        break;
        case SOBREPOE:
            teste_sobreposicao(VETOR_LISTAS,inst,arg);
        break;
        case CHECA_PONTO:
            checar_ponto(VETOR_LISTAS[FIGS],inst,arg);
        break;
        case DISTANCIA_CENTRO:
            distancia_figuras(VETOR_LISTAS[FIGS],inst,arg);
        break;
        case DIST_COMPARADA_CENTRO:
            distancia_comparada_figuras(VETOR_LISTAS[FIGS],inst,arg);
        break;
        case PARADA:
            arg->parada = 1;
        break;
    }
}

int receberComando(char *inst,int tipoArq)
{
    char* aux; 
    int i=0,n = strlen(inst);
    aux = alocarMemoria(4);
    for(i=0;i<n;i++)
    {
        if(inst[i] != ' ' && inst[i] != '\n')
        {
            aux[i]= inst[i];
        }
        else
        {
            break;
        }
    }
    if(tipoArq == GEO_FILE){
        if(strcmp(aux,"q") == 0)
        {
            free(aux);
            return CRIA_QUADRA;
        }
        if(strcmp(aux,"h") == 0)
        {
            free(aux);
            return CRIA_HIDRA;
        }
        if(strcmp(aux,"s") == 0)
        {
            free(aux);
            return CRIA_SEMA;
        }
        if(strcmp(aux,"t") == 0)
        {
            free(aux);
            return CRIA_RADIO;
        }
        if(strcmp(aux,"cq") == 0)
        {
            free(aux);
            return COR_QUADRA;
        }
        if(strcmp(aux,"ch") == 0)
        {
            free(aux);
            return COR_HIDRA;
        }
        if(strcmp(aux,"cs") == 0)
        {
            free(aux);
            return COR_SEMA;
        }
        if(strcmp(aux,"ct") == 0)
        {
            free(aux);
            return COR_RADIO;
        }
        if(strcmp(aux,"nx") == 0)
        {
            free(aux);
            return ALTERAR_MAX;
        }
        if(strcmp(aux,"c") == 0)
        {
            free(aux);
            return CRIA_CIRCULO;
        }
        if(strcmp(aux,"r") == 0)
        {
            free(aux);
            return CRIA_RETANGULO;
        }
        if(strcmp(aux,"o") == 0)
        {
            free(aux);
            return SOBREPOE;
        }
        if(strcmp(aux,"i") == 0)
        {
            free(aux);
            return CHECA_PONTO;
        }
        if(strcmp(aux,"d") == 0)
        {
            free(aux);
            return DISTANCIA_CENTRO;
        }
        if(strcmp(aux,"a") == 0)
        {
            free(aux);
            return DIST_COMPARADA_CENTRO;
        }
        if(strcmp(aux,"#") == 0)
        {
            free(aux);
            return PARADA;
        }
    }
    
    if(tipoArq == QRY_FILE) {  
        if(strcmp(aux,"q?") == 0)
        {
            free(aux);
            return REPORTA_RET;
        }
        if(strcmp(aux,"Q?") == 0)
        {
            free(aux);
            return REPORTA_CIRC;
        }
        if(strcmp(aux,"dq") == 0)
        {
            free(aux);
            return REMOVE_QUADRA_RET;
        }
        if(strcmp(aux,"dle") == 0)
        {
            free(aux);
            return REMOVE_EQUIP_RET;
        }
        if(strcmp(aux,"Dq") == 0)
        {
            free(aux);
            return REMOVE_QUADRA_CIRC;
        }
        if(strcmp(aux,"Dle") == 0)
        {
            free(aux);
            return REMOVE_EQUIP_CIRC;
        }
        if(strcmp(aux,"cc") == 0)
        {
            free(aux);
            return MUDAR_COR;
        }
        if(strcmp(aux,"crd?") == 0)
        {
            free(aux);
            return INFO_TUDO;
        }
        if(strcmp(aux,"crb?") == 0)
        {
            free(aux);
            return MENOR_DIST;
        }
    }
    
    if(tipoArq == EC_FILE){
        if(strcmp(aux,"t") == 0)
        {
            free(aux);
            return DEFINE_ESTABELECIMENTO;
        }
        if(strcmp(aux,"e") == 0)
        {
            free(aux);
            return INSERE_ESTABELECIMENTO;
        }
    }
    
    if(tipoArq == PM_FILE){    
        if(strcmp(aux,"p") == 0)
        {
            free(aux);
            return INSERE_PESSOA;
        }
        if(strcmp(aux,"m") == 0)
        {
            free(aux);
            return INFORMA_PESSOA;
        }
    }

}
//? CASO SEJA UMA TORRE BASE, RECEBE A LISTA DE TORRES,RECEBE A ARVORE DE ELEMENTOS,  RECEBE O TIPO DE EQ, RECEBE A LINHA DE INSTRUÇÃO E RECEBE O VETOR DE STRUCT "Colors"
void criarEquipamentoUrbano(Lista EQ_ESCOLHIDO,Kdtree ELEMENTOS,int tipo, char* inst, Colors* col)
{
    char* aux,*aux2;
    Posic p;
    aux = alocarMemoria( strlen(inst));
    aux2 = alocarMemoria( strlen(inst));
    strcpy(aux,inst);

    aux2 = separate_char(aux,aux2);

    switch(tipo)
    {
        case CRIA_QUADRA:
            struct Quadra* quad = (struct Quadra*)newQuadra();
            
            aux2 = separate_char(aux,aux2);
            quad->id = alocarMemoria( strlen(aux2));
            strcpy(quad->id,aux2);
            
            aux2 = separate_char(aux,aux2);
            quad->x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            quad->y = atof(aux2);

            aux2 = separate_char(aux,aux2);
            quad->width = atof(aux2);

            aux2 = separate_char(aux,aux2);
            quad->height = atof(aux2);

            quad->fillColor = alocarMemoria( strlen(col->corFill));
            quad->perColor = alocarMemoria( strlen(col->corBorder));
            strcpy(quad->fillColor , col->corFill);
            strcpy(quad->perColor , col->corBorder);
            insert_arvore(ELEMENTOS,(Item) quad);

        break;
        case CRIA_HIDRA:
            struct Hidrante *hid = (struct Hidrante*)newHidrante;

            aux2 = separate_char(aux,aux2);
            hid->id = alocarMemoria( strlen(aux2));
            strcpy(hid->id,aux2);

            aux2 = separate_char(aux,aux2);
            hid->x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            hid->y = atof(aux2);

            hid->fillColor = alocarMemoria( strlen(col->corFill));
            hid->perColor = alocarMemoria( strlen(col->corBorder));
            strcpy(hid->fillColor , col->corFill);
            strcpy(hid->perColor , col->corBorder);
            insert_arvore(ELEMENTOS,(Item) hid);

        break;
        case CRIA_SEMA:
            struct Semaforo *sem = (struct Semaforo*)newSemaforo;

            aux2 = separate_char(aux,aux2);
            sem->id = alocarMemoria( strlen(aux2));
            strcpy(sem->id,aux2);

            aux2 = separate_char(aux,aux2);
            sem->x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            sem->y = atof(aux2);

            sem->fillColor = alocarMemoria( strlen(col->corFill));
            sem->perColor = alocarMemoria( strlen(col->corBorder));
            strcpy(sem->fillColor , col->corFill);
            strcpy(sem->perColor , col->corBorder);
            insert_arvore(ELEMENTOS,(Item) sem);

        break;
        case CRIA_RADIO:
            insert_lista(EQ_ESCOLHIDO,(Item) newRadio());
            p = get_last_lista(EQ_ESCOLHIDO);
            struct Radio *rad = (struct Radio*)get_lista(EQ_ESCOLHIDO,p);


            aux2 = separate_char(aux,aux2);
            rad->id = alocarMemoria( strlen(aux2));
            strcpy(rad->id,aux2);

            aux2 = separate_char(aux,aux2);
            rad->x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            rad->y = atof(aux2);

            rad->fillColor = alocarMemoria( strlen(col->corFill));
            rad->perColor = alocarMemoria( strlen(col->corBorder));
            strcpy(rad->fillColor, col->corFill);
            strcpy(rad->perColor ,col->corBorder);
            insert_arvore(ELEMENTOS,(Item) rad);
            
        break;
    }
    free(aux);
    free(aux2);
}

void alterarCorEU(int tipo, char* inst, Colors** col)
{
    char* aux;
    char* aux2;
    if(col[tipo] == NULL)
    {
        col[tipo] = newColors();
    }

    aux = alocarMemoria( strlen(inst));
    aux2 = alocarMemoria( strlen(inst));
    strcpy(aux,inst);

    aux = cut_until_char(aux);
    aux2 = separate_char(aux,aux2);
    if(col[tipo]->corBorder == NULL)
    {
        col[tipo]->corBorder = (char*)calloc(strlen(aux2),sizeof(char));
    }
    else
    {
        free(col[tipo]->corBorder);
        col[tipo]->corBorder = (char*)calloc(strlen(aux2),sizeof(char));
    }
    strcpy(col[tipo]->corBorder,aux2);
    
    aux2 = separate_char(aux,aux2);
    if(col[tipo]->corFill == NULL)
    {
        col[tipo]->corFill = (char*)calloc(strlen(aux2),sizeof(char));
    }
    else
    {
        free(col[tipo]->corFill);
        col[tipo]->corFill = (char*)calloc(strlen(aux2),sizeof(char));
    }
    strcpy(col[tipo]->corFill,aux2);
    free(aux);
    free(aux2);
}

void reportarFiguras(Lista* VETOR_LISTAS,int tipo ,char* inst,Arguments* arg)
{
    if (tipo == REPORTA_RET)
    {
        localizarArea(VETOR_LISTAS,inst,RETANGULO,NADA,arg);
    }else if(tipo == REPORTA_CIRC)
    {
        localizarArea(VETOR_LISTAS,inst,CIRCULO,NADA,arg);
    }
}

void removeObjeto(Lista* VETOR_LISTAS,int tipo,char* inst,Arguments* arg)
{

    switch(tipo)
    {
        case REMOVE_QUADRA_RET:
            localizarArea(VETOR_LISTAS,inst,RETANGULO,SIM,arg);
        break;
        case REMOVE_EQUIP_RET:
            localizarArea(VETOR_LISTAS,inst,RETANGULO,NAO,arg);
        break;
        case REMOVE_QUADRA_CIRC:
            localizarArea(VETOR_LISTAS,inst,CIRCULO,SIM,arg);
        break;
        case REMOVE_EQUIP_CIRC:
            localizarArea(VETOR_LISTAS,inst,CIRCULO,NAO,arg);
        break;
    }
}
void mudarCor(Lista* VETOR_LISTAS, char* inst,Colors** col)
{
    int i,j;
    Posic p;
    void* objeto;
    char* aux,*corF;
    char* aux2,*corB;
    char* id;
    //
        aux = alocarMemoria( strlen(inst));
        aux2 = alocarMemoria( strlen(inst));
        corF = alocarMemoria( strlen(inst));
        corB = alocarMemoria( strlen(inst));
        id = alocarMemoria( strlen(inst));

        strcpy(aux,inst);

        aux = cut_until_char(aux);
        aux2 = separate_char(aux,aux2);
        strcpy(id,aux2);
        aux2 = separate_char(aux,aux2);
        strcpy(corB,aux2);
        aux2 = separate_char(aux,aux2);
        strcpy(corF,aux2);
    //
    for(i=0;i < 4;i++)
    {
        p = get_first_lista(VETOR_LISTAS[i]);
        while(p != NULL)
        {
            objeto = get_lista(VETOR_LISTAS[i],p);
            switch(i)
            {
                case QUADRA:
                    if( strcmp(((struct Quadra*)objeto)->id,id)==0)
                    {
                        strcpy(((struct Quadra*)objeto)->fillColor,corF);
                        strcpy(((struct Quadra*)objeto)->perColor,corB);
                        return;
                    }
                break;
                case HIDRANTE:
                    if( strcmp(((struct Hidrante*)objeto)->id,id)==0)
                    {
                        strcpy(((struct Hidrante*)objeto)->fillColor,corF);
                        strcpy(((struct Hidrante*)objeto)->perColor,corB);
                        return;
                    }
                break;
                case SEMAFORO:
                    if( strcmp(((struct Semaforo*)objeto)->id,id)==0)
                    {
                        strcpy(((struct Semaforo*)objeto)->fillColor,corF);
                        strcpy(((struct Semaforo*)objeto)->perColor,corB);
                        return;
                    }
                break;
                case TORRE_RADIO:
                    if( strcmp(((struct Radio*)objeto)->id,id)==0)
                    {
                        strcpy(((struct Radio*)objeto)->fillColor,corF);
                        strcpy(((struct Radio*)objeto)->perColor,corB);
                        return;
                    }
                break;
            }
            p = get_next_lista(VETOR_LISTAS[i],p);
        }
    }
    free(aux);
    free(aux2);
    free(corB);
    free(corF);
}

void procurarObjeto(Lista* VETOR_LISTAS, char* inst,Arguments* arg)
{
    int i,j,bol=0;
    Posic p;
    void* objeto;
    char* aux;
    char* aux2;
    char* id;
    
    aux = alocarMemoria( strlen(inst));
    aux2 = alocarMemoria( strlen(inst));
    id = alocarMemoria( strlen(inst));

    strcpy(aux,inst);

    aux = cut_until_char(aux);
    aux = cut_until_char(aux);
    aux2 = separate_char(aux,aux2);//talvez de erro
    strcpy(id,aux2);

    for(i=0;i < 4;i++)
    {
        p = get_first_lista(VETOR_LISTAS[i]);
        while(p != NULL)
        {
            objeto = get_lista(VETOR_LISTAS[i],p);
            switch(i)
            {
                case QUADRA:
                    if( strcmp(((struct Quadra*)objeto)->id,id)==0)
                    {
                        fprintf(arg->txtFile,"QUADRA ENCONTRADA = %s\n",((struct Quadra*)objeto)->id);
                        bol = 1;
                    }
                break;
                case HIDRANTE:
                    if( strcmp(((struct Hidrante*)objeto)->id,id)==0)
                    {   
                        fprintf(arg->txtFile,"HIDRANTE ENCONTRADO = %s\n",((struct Hidrante*)objeto)->id);   
                        bol = 1;
                    }
                break;
                case SEMAFORO:
                    if( strcmp(((struct Semaforo*)objeto)->id,id)==0)
                    {
                        fprintf(arg->txtFile,"SEMAFORO ENCONTRADO = %s\n",((struct Semaforo*)objeto)->id);   
                        bol = 1;
                    }
                break;
                case TORRE_RADIO:
                    if( strcmp(((struct Radio*)objeto)->id,id)==0)
                    {
                        fprintf(arg->txtFile,"TORRE_RADIO ENCONTRADO = %s\n",((struct Radio*)objeto)->id);   
                        bol = 1;
                    }
                break;
            }
            p = get_next_lista(VETOR_LISTAS[i],p);
            if(bol == 1)
            {
                break;
            }
        }
        if(bol == 1)
        {
            break;
        }
    }
    free(aux);
    free(aux2);
}

//* FUNÇÕES DO T2
    void teste_sobreposicao(Lista* FIGURAS_LISTA,char* inst,Arguments* arg)
    {
        Posic p;
        void* figura1;
        void* figura2;
        char* aux;
        char* aux2;

        char* id1;
        char* id2;

        int valor;

        aux = alocarMemoria( strlen(inst));
        aux2 = alocarMemoria( strlen(inst));
        
        strcpy(aux,inst);
        aux = cut_until_char(aux);
        
        aux2 = separate_char(aux,aux2);
        id1 = alocarMemoria( strlen(aux2));
        strcpy(id1,aux2);
        
        aux2 = separate_char(aux,aux2);
        id2 = alocarMemoria( strlen(aux2));
        strcpy(id2,aux2);

        p = get_first_lista(FIGURAS_LISTA[FIGS]);
        while(p)//achando figura de id1
        {
            figura1 = get_lista(FIGURAS_LISTA[FIGS],p);
            if( strcmp(((Figure*)figura1)->id,id1) == 0 || strcmp(((Figure*)figura1)->id,id2) == 0 )
            {
                p = get_next_lista(FIGURAS_LISTA[FIGS],p);
                break;
            }
            p = get_next_lista(FIGURAS_LISTA[FIGS],p);
        }
        while(p)//achando figura de id2
        {
            figura2 = get_lista(FIGURAS_LISTA[FIGS],p);
            if( strcmp(((Figure*)figura2)->id,id1) == 0 || strcmp(((Figure*)figura2)->id,id2) == 0 )
            {
                break;
            }
            p = get_next_lista(FIGURAS_LISTA[FIGS],p);
        }
        valor = checando_sobreposicao(figura1,figura2,inst);
        strcpy(aux,inst);
        if(valor != 0)
        {
            strcat(aux,"\n");
            fprintf(arg->txtFile,"%s",aux);
            fprintf(arg->txtFile,"%s","SIM\n");
            sobrepoe_borda(FIGURAS_LISTA,figura1,figura2); 
        }
        else
        {
            strcat(aux,"\n");
            fprintf(arg->txtFile,"%s",aux);
            fprintf(arg->txtFile,"%s","NAO\n");
        }
        free(aux);
        free(aux2);
    }

    int checando_sobreposicao(void* figura1,void* figura2,char* inst)
    {

        float dist,menor;
        /*inicializando o menor valor*/
        if( ((Figure*)figura1)->radius!=0)
        {
            menor =  ((Figure*)figura1)->radius* ((Figure*)figura1)->radius;
        }
        else
        {
            menor =  ((Figure*)figura2)->radius* ((Figure*)figura2)->radius;
        }

        /*retangulo-retangulo*/
        if( ((Figure*)figura1)->radius==0 &&  ((Figure*)figura2)->radius == 0)
        {

            /* ponto esquerda-cima*/
            if( ((Figure*)figura1)->x >=  ((Figure*)figura2)->x 
            &&  ((Figure*)figura1)->x <=  ((Figure*)figura2)->x+ ((Figure*)figura2)->width
            &&  ((Figure*)figura1)->y >=  ((Figure*)figura2)->y 
            &&  ((Figure*)figura1)->y <=  ((Figure*)figura2)->y+ ((Figure*)figura2)->height){
            
                return 1;
            }
            else
            {
                /* ponto direita cima*/
                if( ((Figure*)figura1)->x+ ((Figure*)figura1)->width >=  ((Figure*)figura2)->x 
                &&  ((Figure*)figura1)->x+ ((Figure*)figura1)->width <=  ((Figure*)figura2)->x+ ((Figure*)figura2)->width
                &&  ((Figure*)figura1)->y >=  ((Figure*)figura2)->y 
                &&  ((Figure*)figura1)->y <=  ((Figure*)figura2)->y+ ((Figure*)figura2)->height)
                {
                
                    return 1;	
                }
                else
                {
                    /* ponto esquerda baixo*/
                    if( ((Figure*)figura1)->x >=  ((Figure*)figura2)->x 
                    &&  ((Figure*)figura1)->x <=  ((Figure*)figura2)->x+ ((Figure*)figura2)->width
                    &&  ((Figure*)figura1)->y+ ((Figure*)figura1)->height >=  ((Figure*)figura2)->y 
                    &&  ((Figure*)figura1)->y+ ((Figure*)figura1)->height <=  ((Figure*)figura2)->y+ ((Figure*)figura2)->height)
                    {

                        return 1;	
                    }
                    else
                    {
                        /* ponto direita baixo*/
                        if( ((Figure*)figura1)->x+ ((Figure*)figura1)->width >=  ((Figure*)figura2)->x 
                        &&  ((Figure*)figura1)->x+ ((Figure*)figura1)->width <=  ((Figure*)figura2)->x+ ((Figure*)figura2)->width
                        &&  ((Figure*)figura1)->y+ ((Figure*)figura1)->height >=  ((Figure*)figura2)->y 
                        &&  ((Figure*)figura1)->y+ ((Figure*)figura1)->height <=  ((Figure*)figura2)->y+ ((Figure*)figura2)->height)
                        {
                        
                            return 1;
                        }
                        else
                        {
                            if( ((Figure*)figura2)->x >=  ((Figure*)figura1)->x 
                            &&  ((Figure*)figura2)->x <=  ((Figure*)figura1)->x+ ((Figure*)figura1)->width
                            &&  ((Figure*)figura2)->y >=  ((Figure*)figura1)->y 
                            &&  ((Figure*)figura2)->y <=  ((Figure*)figura1)->y+ ((Figure*)figura1)->height)
                            {
                                return 1;
                            }
                            else
                            {
                                /* ponto direita cima*/
                                if( ((Figure*)figura2)->x+ ((Figure*)figura2)->width >=  ((Figure*)figura1)->x 
                                &&  ((Figure*)figura2)->x+ ((Figure*)figura2)->width <=  ((Figure*)figura1)->x+ ((Figure*)figura1)->width
                                &&  ((Figure*)figura1)->y >=  ((Figure*)figura1)->y 
                                &&  ((Figure*)figura1)->y <=  ((Figure*)figura1)->y+ ((Figure*)figura1)->height)
                                {
                                
                                    return 1;	
                                }
                                else
                                {
                                    /* ponto esquerda baixo*/
                                    if( ((Figure*)figura2)->x >=  ((Figure*)figura1)->x 
                                    &&  ((Figure*)figura2)->x <=  ((Figure*)figura1)->x+ ((Figure*)figura1)->width
                                    &&  ((Figure*)figura2)->y+ ((Figure*)figura2)->height >=  ((Figure*)figura1)->y 
                                    &&  ((Figure*)figura2)->y+ ((Figure*)figura2)->height <=  ((Figure*)figura1)->y+ ((Figure*)figura1)->height)
                                    {

                                        return 1;	
                                    }
                                    else
                                    {
                                        /* ponto direita baixo*/
                                        if( ((Figure*)figura2)->x+ ((Figure*)figura2)->width >=  ((Figure*)figura1)->x 
                                        &&  ((Figure*)figura2)->x+ ((Figure*)figura2)->width <=  ((Figure*)figura1)->x+ ((Figure*)figura1)->width
                                        &&  ((Figure*)figura2)->y+ ((Figure*)figura2)->height >=  ((Figure*)figura1)->y 
                                        &&  ((Figure*)figura2)->y+ ((Figure*)figura2)->height <=  ((Figure*)figura1)->y+ ((Figure*)figura1)->height)
                                        {
                                        
                                            return 1;
                                        }
                                    return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
        /*circulo-circulo*/ /*check*/
        if( ((Figure*)figura1)->radius!=0 &&  ((Figure*)figura2)->radius != 0)
        {
            dist = dotDistance( ((Figure*)figura1)->xcenter,((Figure*)figura1)->ycenter,((Figure*)figura2)->xcenter,((Figure*)figura2)->ycenter);
            if (dist <=  ((Figure*)figura1)->radius +  ((Figure*)figura2)->radius)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        
        }
        else
        {
        /*circulo-retangulo*/ /*check*/
        if( ((Figure*)figura1)->radius!=0) /*se cond for verdadeira, id1 é um círculo, e id2 é um retangulo*/
        {
            dist = dotDistance( ((Figure*)figura1)->x, ((Figure*)figura1)->y, ((Figure*)figura2)->x, ((Figure*)figura2)->y);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura1)->x, ((Figure*)figura1)->y, ((Figure*)figura2)->x +  ((Figure*)figura2)->width, ((Figure*)figura2)->y);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura1)->x, ((Figure*)figura1)->y, ((Figure*)figura2)->x, ((Figure*)figura2)->y +  ((Figure*)figura2)->height);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura1)->x, ((Figure*)figura1)->y, ((Figure*)figura2)->x +  ((Figure*)figura2)->width, ((Figure*)figura2)->y +  ((Figure*)figura2)->height);
            if(dist < menor)
            {
                menor = dist;
            }

            dist = dotDistance( ((Figure*)figura1)->x, ((Figure*)figura1)->y, ((Figure*)figura2)->xcenter, ((Figure*)figura2)->ycenter);
            if(dist < menor)
            {
                menor = dist;
            }
            
            if(menor< ((Figure*)figura1)->radius)
            {
                return 3;
            }
            else
            {
                return 0;
            }
        }
        else /*se cond for falsa, id2 é um círculo, e id1 é um retangulo*/
        {
            dist = dotDistance( ((Figure*)figura2)->x, ((Figure*)figura2)->y, ((Figure*)figura1)->x, ((Figure*)figura1)->y);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura2)->x, ((Figure*)figura2)->y, ((Figure*)figura1)->x +  ((Figure*)figura1)->width, ((Figure*)figura1)->y);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura2)->x, ((Figure*)figura2)->y, ((Figure*)figura1)->x, ((Figure*)figura1)->y +  ((Figure*)figura1)->height);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura2)->x, ((Figure*)figura2)->y, ((Figure*)figura1)->x +  ((Figure*)figura1)->width, ((Figure*)figura1)->y +  ((Figure*)figura1)->height);
            if(dist < menor)
            {
                menor = dist;
            }
            dist = dotDistance( ((Figure*)figura2)->x, ((Figure*)figura2)->y, ((Figure*)figura1)->xcenter, ((Figure*)figura1)->ycenter);
            if(dist < menor)
            {
                menor = dist;
            }
            if(menor< ((Figure*)figura2)->radius)
            {
                return 3;
            }
            else
            {
                return 0;
            }
        }
        }
        }
    }
    void sobrepoe_borda(Lista* FIGURAS_LISTA,void* figura1,void* figura2)
    {
        Posic p;
        void* figura;
        float x, y, h, w;

        if( ((Figure*)figura1)->radius == 0)/*CASO VERDADE SERA UM RETANGULO*/
        {
            if( ((Figure*)figura2)->radius == 0)/*CASO VERDADE, AS DUAS FIG SERÃO RETANGULOS*/
            {
                x = min( ((Figure*)figura1)->x,  ((Figure*)figura2)->x);
                y = min( ((Figure*)figura1)->y,  ((Figure*)figura2)->y);

                w = max( ((Figure*)figura1)->x +  ((Figure*)figura1)->width,  ((Figure*)figura2)->x +  ((Figure*)figura2)->width);
                h = max( ((Figure*)figura1)->y +  ((Figure*)figura1)->height,  ((Figure*)figura2)->y +  ((Figure*)figura2)->height);

            }
            else/*CASO VERDADE, FIG1 É RETANGULO E FIG2 É CIRCULO*/
            {
                x = min( ((Figure*)figura1)->x,  ((Figure*)figura2)->x -  ((Figure*)figura2)->radius);
                y = min( ((Figure*)figura1)->y,  ((Figure*)figura2)->y -  ((Figure*)figura2)->radius);

                w = max( ((Figure*)figura1)->x +  ((Figure*)figura1)->width,  ((Figure*)figura2)->x +  ((Figure*)figura2)->radius);
                h = max( ((Figure*)figura1)->y +  ((Figure*)figura1)->height,  ((Figure*)figura2)->y +  ((Figure*)figura2)->radius);

            }
        }
        else
        {
            if( ((Figure*)figura2)->radius == 0)/*CASO VERDADE, FIG1 É CIRCULO E FIG2 É RETANGULO*/
            {
                x = min( ((Figure*)figura2)->x,  ((Figure*)figura1)->x -  ((Figure*)figura1)->radius);
                y = min( ((Figure*)figura2)->y,  ((Figure*)figura1)->y -  ((Figure*)figura1)->radius);

                w = max( ((Figure*)figura2)->x +  ((Figure*)figura2)->width,  ((Figure*)figura1)->x +  ((Figure*)figura1)->radius);
                h = max( ((Figure*)figura2)->y +  ((Figure*)figura2)->height,  ((Figure*)figura1)->y +  ((Figure*)figura1)->radius);
            }
            else/*CASO VERDADE, AS DUAS FIG SERÃO CIRCULOS*/
            {
                x = min( ((Figure*)figura1)->x -  ((Figure*)figura1)->radius,  ((Figure*)figura2)->x -  ((Figure*)figura2)->radius);
                y = min( ((Figure*)figura1)->y -  ((Figure*)figura1)->radius,  ((Figure*)figura2)->y -  ((Figure*)figura2)->radius);

                w = max( ((Figure*)figura1)->x +  ((Figure*)figura1)->radius,  ((Figure*)figura2)->x +  ((Figure*)figura2)->radius);
                h = max( ((Figure*)figura1)->y +  ((Figure*)figura1)->radius,  ((Figure*)figura2)->y +  ((Figure*)figura2)->radius);
            }
        }

        w-=x;
        h-=y;

        x-=1;
        y-=1;

        w += 2 * 1;
        h += 2 * 1;
        insert_lista(FIGURAS_LISTA[FIGS_EXTRA],(Figure*)newFigure());
        p = get_last_lista(FIGURAS_LISTA[FIGS_EXTRA]);
        figura = get_lista(FIGURAS_LISTA[FIGS_EXTRA],p);
        ((Figure*)figura)->x = x;
        ((Figure*)figura)->y = y;
        ((Figure*)figura)->width = w;
        ((Figure*)figura)->height = h;
        ((Figure*)figura)->id = alocarMemoria(9);
        strcpy( ((Figure*)figura)->id,"SOBREPOE");

    }

    void checar_ponto(Lista FIGURAS_LISTA,char* inst,Arguments* arg)
    {
        Posic p;
        void* figura;
        char* aux;
        char* aux2;
        float x,y;
        char* id;

        aux =  alocarMemoria( strlen(inst));
        aux2 =  alocarMemoria( strlen(inst));
        strcpy(aux,inst);

        aux = cut_until_char(aux);
        aux2 = separate_char(aux,aux2);
        
        p = get_first_lista(FIGURAS_LISTA);
        figura = get_lista(FIGURAS_LISTA,p);
        while(p)
        {
            if( strcmp(((Figure*)figura)->id,aux2) == 0 )
            {
                aux2 = separate_char(aux,aux2);
                x = atof(aux2);
                aux2 = separate_char(aux,aux2);
                y = atof(aux2);

                if(x >= ((Figure*)figura)->x && x <= ((Figure*)figura)->x + ((Figure*)figura)->width)
                {
                    if(y >= ((Figure*)figura)->y && y <= ((Figure*)figura)->y + ((Figure*)figura)->height)
                    {
                        fprintf(arg->txtFile,"%s\n",inst);
                        fprintf(arg->txtFile,"SIM\n");
                        break;
                    }
                }

                fprintf(arg->txtFile,"%s\n",inst);
                fprintf(arg->txtFile,"NAO\n");
                break;
            }
            p = get_next_lista(FIGURAS_LISTA,p);
            figura = get_lista(FIGURAS_LISTA,p);
        }
        free(aux);
    }

    void distancia_figuras(Lista FIGURAS_LISTA,char* inst,Arguments* arg)
    {
        Posic p;
        void* figura1;
        void* figura2;
        char* aux;
        char* aux2;
        char* id1,*id2;
        float dist;

        aux =  alocarMemoria( strlen(inst));
        aux2 =  alocarMemoria( strlen(inst));
        strcpy(aux,inst);

        aux = cut_until_char(aux);
        aux2 = separate_char(aux,aux2);
        id1 = alocarMemoria( strlen(aux2));
        strcpy(id1,aux2);
        
        aux2 = separate_char(aux,aux2);
        id2 = alocarMemoria( strlen(aux2));
        strcpy(id2,aux2);
        
        p = get_first_lista(FIGURAS_LISTA);
        figura1 = get_lista(FIGURAS_LISTA,p);
        while(p)//achando figura de id1
        {
            if( strcmp(((Figure*)figura1)->id,id1) == 0 || strcmp(((Figure*)figura1)->id,id2) == 0 )
            {
                if(strcmp(id1,id2)!=0)
                {
                    p = get_next_lista(FIGURAS_LISTA,p);
                }
                break;
            }
            p = get_next_lista(FIGURAS_LISTA,p);
            figura1 = get_lista(FIGURAS_LISTA,p);
        }
        figura2 = get_lista(FIGURAS_LISTA,p);
        while(p)//achando figura de id2
        {
            if( strcmp(((Figure*)figura2)->id,id1) == 0 || strcmp(((Figure*)figura2)->id,id2) == 0 )
            {
                break;
            }
            p = get_next_lista(FIGURAS_LISTA,p);
            figura2 = get_lista(FIGURAS_LISTA,p);
        }

        dist = dotDistance( ((Figure*)figura1)->xcenter,((Figure*)figura1)->ycenter,((Figure*)figura2)->xcenter,((Figure*)figura2)->ycenter);

        fprintf(arg->txtFile,"%s",inst);
        fprintf(arg->txtFile,"%f\n",dist);
        free(aux);
        free(aux2);
    }

    void distancia_comparada_figuras(Lista FIGURAS_LISTA,char* inst,Arguments* arg)
    {
        Posic p;
        void* figura1;
        void* figura2;
        char* aux;
        char* aux2;
        char* aux3;
        char* id;
        char* nomesvg;

        float dist;
        FILE* svg;
        int j,i,n=0;

        aux =  alocarMemoria( strlen(inst));
        aux2 =  alocarMemoria( strlen(inst));
        strcpy(aux,inst);

        aux = cut_until_char(aux);
        aux2 = separate_char(aux,aux2);
        id =  alocarMemoria( strlen(inst));
        strcpy(id,aux2);
        
        p = get_first_lista(FIGURAS_LISTA);
        while(p)//achando figura de "id"
        {
            figura1 = get_lista(FIGURAS_LISTA,p);
            if( strcmp(((Figure*)figura1)->id,id) == 0)
            {
                break;
            }
            p = get_next_lista(FIGURAS_LISTA,p);
        }
        aux2 = separate_char(aux,aux2);
        //ajustando o nome do svg criado por "a"
            nomesvg = alocarMemoria( strlen(aux2)+ strlen(arg->NameGeoFile)+ strlen(arg->directory));
            char* auxiliar;
            char* nomeG=(char*)calloc(strlen(arg->NameGeoFile)+1,sizeof(char)) ; 
            
            strcpy(nomeG,arg->NameGeoFile);

            auxiliar = alocarMemoria( strlen(arg->directory));
            strcpy(auxiliar,arg->directory);
            
            i = strlen(nomeG)-1;
            j = i-4;
            while(j!=0)
            {
                if(nomeG[j]=='/')
                {
                    shiftLeft(nomeG,j);
                    break;
                }
                j--;
            }
            j=i-4;
            nomeG[j+1] = 0;
            nomeG[j+2] = 0;
            nomeG[j+3] = 0;

            aux3 = alocarMemoria(5 + strlen(nomeG) + strlen(aux2));

            sprintf(aux3, "%s-%s.svg", nomeG, aux2);
            nomesvg = alocarMemoria( strlen(aux3)+ strlen(auxiliar));
            strcat(nomesvg,auxiliar);
        //
        strcat(nomesvg,aux3);
        abrindoArquivo(&svg,nomesvg,SVG);

        openingSVG(svg);
        p = get_first_lista(FIGURAS_LISTA);
        while(p) //desenhando figuras
        {
            figura2 = get_lista(FIGURAS_LISTA,p);
            if( ((Figure*)figura2)->radius == 0)
            {
                fprintf(svg,"%s%.1f%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<rect x=\"",((Figure*)figura2)->x,"\" y=\"",((Figure*)figura2)->y,"\" width=\"",((Figure*)figura2)->width,"\" height=\"",((Figure*)figura2)->height,"\" style=\"fill:",((Figure*)figura2)->fillColor,";stroke-width:1;stroke:",((Figure*)figura2)->perColor,";opacity:0.4;\" />\n"); /*printando um retangulo*/
            }
            else
            {
                fprintf(svg,"%s%.1f%s%.1f%s%.1f%s%s%s%s%s","<circle cx=\"",((Figure*)figura2)->x,"\" cy=\"",((Figure*)figura2)->y,"\" r=\"",((Figure*)figura2)->radius,"\" style=\"fill:",((Figure*)figura2)->fillColor,";stroke-width:1;stroke:",((Figure*)figura2)->perColor,";opacity:0.4;\" />\n");
            }
            p = get_next_lista(FIGURAS_LISTA,p);   
        }
        p = get_first_lista(FIGURAS_LISTA);
        while(p) //desenhando linhas
        {
            figura2 = get_lista(FIGURAS_LISTA,p);
            if( strcmp(((Figure*)figura2)->id,((Figure*)figura1)->id) != 0)
            {
                dist = dotDistance(((Figure*)figura1)->xcenter,((Figure*)figura1)->ycenter,((Figure*)figura2)->xcenter,((Figure*)figura2)->ycenter);
                
                fprintf(svg,"<line x1=\"%.1f\" y1=\"%.1f\" x2=\"%.1f\" y2=\"%.1f\" style=\"stroke:%s;stroke-width:1\" />\n", /*PRINTANDO AS LINHAS*/
                ((Figure*)figura1)->xcenter,((Figure*)figura1)->ycenter,((Figure*)figura2)->xcenter,((Figure*)figura2)->ycenter,((Figure*)figura1)->perColor);

                fprintf(svg,"<text x=\"%.1f\" y=\"%.1f\" style=\"fill:%s;font-size:12.0px;font-family:sans-serif\">%.1f</text>\n",
                (((Figure*)figura1)->xcenter+((Figure*)figura2)->xcenter)/2+5,(((Figure*)figura1)->ycenter+((Figure*)figura2)->ycenter)/2+5,((Figure*)figura1)->perColor,dist);
            
            }
            p = get_next_lista(FIGURAS_LISTA,p);
        }
        closingSVG(svg);
        free(aux);
        free(aux2);
        //free(figura1);
        //free(figura2);
        //free(nomesvg);
    }
//