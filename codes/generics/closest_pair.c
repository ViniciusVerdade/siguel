#include "../headers/includes.h"
// A structure to represent a Point in 2D plane
/* Following two functions are needed for library function qsort().
   Refer: http://www.cplusplus.com/reference/clibrary/cstdlib/qsort/ */
 
/*Mergesorts*/

#include <math.h>
typedef struct menordistancia{
    char *id1;
    float x1;
    float y1;
    char *id2;
    float x2;
    float y2;
    float dist;
}Menor_distancia;

Point** transforming_list(Lista LISTRA_REQUERIDA)
{
    Posic p;
    int i=0;
    Point **vetor_pontos = (Point**)calloc(length_lista(LISTRA_REQUERIDA),sizeof(Point*));
    void* objeto;

    p = get_first_lista(LISTRA_REQUERIDA);
    while(p)
    {
        objeto = get_lista(LISTRA_REQUERIDA,p);
        vetor_pontos[i] = (Point*)malloc(sizeof(Point));

        vetor_pontos[i]->x = ((struct Radio*)objeto)->x;
        vetor_pontos[i]->y = ((struct Radio*)objeto)->y;
        vetor_pontos[i]->id = alocarMemoria( strlen( ((struct Radio*)objeto)->id) );
        strcpy(vetor_pontos[i]->id,((struct Radio*)objeto)->id);
        i++;
        p = get_next_lista(LISTRA_REQUERIDA,p);
    }
    return vetor_pontos;
}


void margex(Point **vetor, int comeco, int meio, int fim)
{
    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;
    Point **vetAux;
    vetAux = malloc(tam * sizeof(Point*));

    while(com1 <= meio && com2 <= fim){
        if(vetor[com1]->x < vetor[com2]->x) {
            vetAux[comAux] = vetor[com1];
            com1++;
        } else {
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }

    while(com1 <= meio){  /*Caso ainda haja elementos na primeira metade*/
        vetAux[comAux] = vetor[com1];
        comAux++;
        com1++;
    }

    while(com2 <= fim) {   /*Caso ainda haja elementos na segunda metade*/
        vetAux[comAux] = vetor[com2];
        comAux++;
        com2++;
    }

    for(comAux = comeco; comAux <= fim; comAux++){    /*Move os elementos de volta para o vetor original*/
        vetor[comAux] = vetAux[comAux-comeco];
    }
    
    free(vetAux);
}

void margey(Point **vetor, int comeco, int meio, int fim) 
{
    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;
    Point **vetAux;
    vetAux = malloc(tam * sizeof(Point*));

    while(com1 <= meio && com2 <= fim){
        if(vetor[com1]->y < vetor[com2]->y) {
            vetAux[comAux] = vetor[com1];
            com1++;
        } else {
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }

    while(com1 <= meio){  /*Caso ainda haja elementos na primeira metade*/
        vetAux[comAux] = vetor[com1];
        comAux++;
        com1++;
    }

    while(com2 <= fim) {   /*Caso ainda haja elementos na segunda metade*/
        vetAux[comAux] = vetor[com2];
        comAux++;
        com2++;
    }

    for(comAux = comeco; comAux <= fim; comAux++){    /*Move os elementos de volta para o vetor original*/
        vetor[comAux] = vetAux[comAux-comeco];
    }
    
    free(vetAux);
}

void margeSorty(Point **vetor, int comeco, int fim)
{
    if (comeco < fim) {
        int meio = (fim+comeco)/2;
        margeSorty(vetor, comeco, meio);
        margeSorty(vetor, meio+1, fim);
        margey(vetor, comeco, meio, fim);
    }
}

void margeSortx(Point **vetor, int comeco, int fim)
{
    if (comeco < fim) {
        int meio = (fim+comeco)/2;
        margeSortx(vetor, comeco, meio);
        margeSortx(vetor, meio+1, fim);
        margex(vetor, comeco, meio, fim);
    }
}

void *CriarMenorDist()
{
    Menor_distancia *menor = malloc(sizeof(Menor_distancia));
    
    menor->id1 = malloc(50 * sizeof(char));

    menor->id2 = malloc(50 * sizeof(char));

 return (void*)menor;
}

/* A divide and conquer program in C++ to find the smallest distance from a
given set of points.*/

/* A utility function to find the distance between two points*/
/*usando a struct point*/
float dist(Point *p1, Point *p2)
{
    return sqrt( (p1->x - p2->x)*(p1->x - p2->x) + (p1->y - p2->y)*(p1->y - p2->y));
}

/* A Brute Force method to return the smallest distance between two points
in P[] of size n*/
void *bruteForce(Point **P, int n, void *menor)
{
    float min = FLT_MAX;
    int i, j;
    j=0;
    for (i = 0; i < n; ++i)
        for (j = i+1; j < n; ++j)
            if (dist(P[i], P[j]) < min){
                min = dist(P[i], P[j]);
                ((Menor_distancia*)menor)->dist = min;
                ((Menor_distancia*)menor)->x1 = P[i]->x;
                ((Menor_distancia*)menor)->y1 = P[i]->y;
                ((Menor_distancia*)menor)->x2 = P[j]->x;
                ((Menor_distancia*)menor)->y2 = P[j]->y;
                strcpy(((Menor_distancia*)menor)->id1,P[i]->id);
                strcpy(((Menor_distancia*)menor)->id2,P[j]->id);
            }
    return menor;
}

/* A utility function to find minimum of two float values*/

/* A utility function to find the distance beween the closest points of
strip of given size. All points in strip[] are sorted accordint to
y coordinate. They all have an upper bound on minimum distance as d.
Note that this method seems to be a O(n^2) method, but it's a O(n)
method as the inner loop runs at most 6 times*/
void *stripClosest(Point **strip,/*tamanho do vetor strip*/ int size, /*tamanho da strip*/float min){
    void *menor = CriarMenorDist();
    int i = 0;
    int j = 1;

    margeSorty(strip, 0, size - 1);
    
    ((Menor_distancia*)menor)->dist = FLT_MAX;
    for (i = 0; i < size; ++i)
        for (j = i+1; j < size && (strip[j]->y - strip[i]->y) < min; ++j)
            if (dist(strip[i], strip[j]) < min)
            {
                min = dist(strip[i], strip[j]);
                ((Menor_distancia*)menor)->dist = min;
                ((Menor_distancia*)menor)->x1 = strip[i]->x;
                ((Menor_distancia*)menor)->y1 = strip[i]->y;
                ((Menor_distancia*)menor)->x2 = strip[j]->x;
                ((Menor_distancia*)menor)->y2 = strip[j]->y;
                strcpy(((Menor_distancia*)menor)->id1,strip[i]->id);
                strcpy(((Menor_distancia*)menor)->id2,strip[j]->id);
            }
    return menor;
}

void *closestUtil(Point **Px, int n)
{
    /* Find the middle point*/
    int mid = n/2;
    void *resposta;
    /* Divide points in y sorted array around the vertical line.
    Assumption: All x coordinates are distinct.*/
    Point **Pyl = malloc(sizeof(Point*) * (mid+1));   /* y sorted points on left of vertical line*/
    Point **Pyr = malloc(sizeof(Point*) * (n-mid-1));  /* y sorted points on right of vertical line*/
    Point *midPoint = Px[mid];
    int li = 0, ri = 0;  /* indexes of left and right subarrays*/
    int i = 0;
    int j = 0;
    void *dl;
    void *dr;
    void *d;
    void *stripClsDist;
    /* Build an array strip[] that contains points close (closer than d)
    to the line passing through the middle point*/
    Point **strip = malloc(n * sizeof(Point*));
    /* If there are 2 or 3 points, then use brute force*/
    void *menor = CriarMenorDist();
    if (n <= 3){
        resposta = bruteForce(Px, n, menor);
        return resposta;
    }

    /* Consider the vertical line passing through the middle point
    calculate the smallest distance dl on left of middle point and
    dr on right side*/
    dl = closestUtil(Px, mid); 
    
    dr = closestUtil(Px, mid);

    /* Find the smaller of two distances*/
    if( ((Menor_distancia*)dl)->dist <= ((Menor_distancia*)dr)->dist)
    {
        d = dl;
    }
    else
    {
        d = dr;
    }


    for (i = 0; i < n; i++)
        if (abs(Px[i]->x - midPoint->x) < ((Menor_distancia*)d)->dist){
            strip[j] = Px[i];
            j++;
        }
    
    stripClsDist = stripClosest(strip, j, ((Menor_distancia*)d)->dist);
    if( ((Menor_distancia*)d)->dist <= ((Menor_distancia*)stripClsDist)->dist)
    {
        resposta = d;
    }
    else
    {
        resposta = stripClsDist;
    }
    /* Find the closest points in strip.  Return the minimum of d and closest
    distance is strip[]*/
    return resposta;
}
    

void closest_pair_lista(Lista* VETOR_LISTAS,Arguments *arg)
{
    int n = length_lista(VETOR_LISTAS[TORRE_RADIO]);
    Menor_distancia *resultado = (Menor_distancia*)malloc(sizeof(Menor_distancia));

    Point **vetor_torres = transforming_list(VETOR_LISTAS[TORRE_RADIO]);
    Point **torres_x = vetor_torres;
    Point **torres_y = vetor_torres;
    margeSortx(torres_x,0,n-1);
    margeSorty(torres_y,0,n-1);
    resultado = closestUtil(torres_x,n);
    
    fprintf(arg->txtFile,"\nTORRES DE MENOR DISTANCIA\nID1 = %s ID2 = %s DISTANCIA = %f",resultado->id1,resultado->id2,resultado->dist);
    Figure *circMarca = (Figure*)malloc(sizeof(Figure));
    
    circMarca->id = alocarMemoria(12);
    strcpy(circMarca->id,"PONTILHADO");
    circMarca->radius = 30;
    circMarca->x = resultado->x1;
    circMarca->y = resultado->y1;
    insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item)circMarca);
    
    circMarca = (Figure*)malloc(sizeof(Figure));
    circMarca->id = alocarMemoria(12);
    strcpy(circMarca->id,"PONTILHADO");
    circMarca->radius = 30;
    circMarca->x = resultado->x2;
    circMarca->y = resultado->y2;
    insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item)circMarca);
}

// void closest_pair_tree(Kdtree _tree,Arguments *arg)
// {
//     int n = length_lista(VETOR_LISTAS[TORRE_RADIO]);
//     Menor_distancia *resultado = (Menor_distancia*)malloc(sizeof(Menor_distancia));

//     Point **vetor_torres = transforming_list(VETOR_LISTAS[TORRE_RADIO]);
//     Point **torres_x = vetor_torres;
//     Point **torres_y = vetor_torres;
//     margeSortx(torres_x,0,n-1);
//     margeSorty(torres_y,0,n-1);
//     resultado = closestUtil(torres_x,n);
    
//     fprintf(arg->txtFile,"\nTORRES DE MENOR DISTANCIA\nID1 = %s ID2 = %s DISTANCIA = %f",resultado->id1,resultado->id2,resultado->dist);
//     Figure *circMarca = (Figure*)malloc(sizeof(Figure));
    
//     circMarca->id = alocarMemoria(12);
//     strcpy(circMarca->id,"PONTILHADO");
//     circMarca->radius = 30;
//     circMarca->x = resultado->x1;
//     circMarca->y = resultado->y1;
//     insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item)circMarca);
    
//     circMarca = (Figure*)malloc(sizeof(Figure));
//     circMarca->id = alocarMemoria(12);
//     strcpy(circMarca->id,"PONTILHADO");
//     circMarca->radius = 30;
//     circMarca->x = resultado->x2;
//     circMarca->y = resultado->y2;
//     insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item)circMarca);
// }

