#include "../headers/generics/hashTable.h"
#include "../headers/generics/uteis.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//* ONDE EXISTE AS INFORMAÇÕES ESTÃO INSERIDAS
struct HashTable{
    struct HashInfo *vetor_info;
    int size;
};

//* CRIANDO A TABELA
HashTable create_table(int size){
    if(size > 0){
        struct HashTable *this = (struct HashTable *) malloc(sizeof(struct HashTable));
        this->vetor_info = (struct HashInfo *) calloc(size,sizeof(struct HashInfo));
        this->size = size;
        return this;
    }else{
        return NULL;
    }
}
//* CRIANDO A CHAVE DA TABELA
int hash_key(char* key,int size,int initi_val){
    int i, value = 0, tamanhoKey = strlen(key);

    for(i = 0; i < tamanhoKey; i++){
        value += key[i] + initi_val;
        initi_val++;
    }

    return value % size;
}

void realloc_table(HashTable _tabela,int new_size){
    struct HashTable* tabela = _tabela; 
    struct HashInfo* vetor_antigo = tabela->vetor_info;
    int size_antigo = tabela->size;
    
    int posic_table;
    int i = 0,j = 0;

    tabela->vetor_info = (struct HashInfo *) calloc(new_size,sizeof(struct HashInfo));
    tabela->size = new_size;

    while(i < size_antigo){
        if(vetor_antigo[i].value){
            j = 0;
            while(true){
                j++;
                posic_table = hash_key( vetor_antigo[i].key , tabela->size , j);
                if(!tabela->vetor_info[posic_table].value){
                    tabela->vetor_info[posic_table] = vetor_antigo[i];
                    break;
                }
            }
        }
        i++;
    }
}

void insert_table(HashTable _tabela, HashInfo info){
    struct HashTable* tabela = _tabela;
    int i = 0;
    
    int posic_table = hash_key(info.key , tabela->size , i);

    if(!tabela->vetor_info[posic_table].value){
        tabela->vetor_info[posic_table] = info;
    }else{
        while(true){
            i++;
            posic_table = hash_key( info.key , tabela->size , i);
            if(!tabela->vetor_info[posic_table].value){
                tabela->vetor_info[posic_table] = info;
                break;
            }
            if(i % tabela->size == 0){
                realloc_table(tabela,tabela->size + 5);
            }
        }
    }
    
}

_Bool if_exist_key(HashTable _tabela, char* key){
    struct HashTable* tabela = _tabela;
    int posic_table = hash_key(key,tabela->size,0);

    if(tabela->vetor_info[posic_table].key){
        return true;
    }else{
        return false;
    }
}

int get_info_table(HashTable _tabela, char* key){
    struct HashTable* tabela = _tabela;
    int i = 0,size = strlen(key);
    int posic_table = hash_key(key,tabela->size,i);

    if( if_exist_key(tabela,key) ){
        if(strcmp(tabela->vetor_info[posic_table].key,key) == 0){
            return posic_table;//! caso voltar a retornar hashinfo "tabela->vetor_info[posic_table]"
        }else{
            while(i < size){
                i++;
                posic_table = hash_key(key,tabela->size,i);
                if(strcmp(tabela->vetor_info[posic_table].key,key) == 0){
                    return posic_table; //! caso voltar a retornar hashinfo "tabela->vetor_info[posic_table]"
                }else if(tabela->vetor_info[posic_table].key == NULL){
                    printf("Ocorreu um erro na checagem de keys\n");
                    return -1;
                }
            }
        }
    }else{
        while(i < size){
            i++;
            posic_table = hash_key(key,tabela->size,i);
            if(strcmp(tabela->vetor_info[posic_table].key,key) == 0){
                return posic_table; //! caso voltar a retornar hashinfo "tabela->vetor_info[posic_table]"
            }else if(tabela->vetor_info[posic_table].key == NULL){
                printf("Ocorreu um erro na checagem de keys\n");
                return -1;
            }
        }
    }
}

HashInfo remove_key(HashTable _tabela, char* key){
    struct HashTable* tabela = _tabela; 
    int posic_table;
    HashInfo infoRemovido;
    
    posic_table = get_info_table(tabela, key);
    infoRemovido = tabela->vetor_info[posic_table];

    tabela->vetor_info[posic_table].key = NULL;
    free(tabela->vetor_info[posic_table].value);

    return infoRemovido;
}

void print_hash_table(HashTable _tabela){
    struct HashTable* tabela = _tabela; 
    printf("\n");
    for(int i = 0; i < tabela->size; i++)  {
        if(tabela->vetor_info[i].value){
            if(tabela->vetor_info[i].key != NULL){
                printf("%s %d\n",tabela->vetor_info[i].key, i);
            }
            
        }
    }
}

