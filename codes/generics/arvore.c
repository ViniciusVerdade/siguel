#include "../headers/generics/arvore.h"
#include "../headers/generics/uteis.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Retorna true se são iguais e false se não for
typedef _Bool (*are_values_equal)(Item a, Item b);
//Deve retornar negativo se for pra esquerda e postivo e maior que zero se for pra direita
typedef int (*which_side)(Item a, Item b); 
//função passada que será executada para cada elemento da tree percorrida
typedef void (*function_during_tree)(Item a,va_list parameters_list);

struct Kdtree {
    Item valor;
    struct Kdtree *left;
    struct Kdtree *right;
    
    are_values_equal checarIgual;// criando uma função checarIguao do tipo are_values_equal
    which_side direcoes[2];// criando uma função que possui duas funções, uma pra direita e outra pra esquerda
};

Kdtree create_arvore(are_values_equal checarIgual, which_side direcoesX, which_side direcoesY) //Quando criar, enviar a função de comparação
{
    struct Kdtree *this = (struct Kdtree *) malloc(sizeof(struct Kdtree));

    this->valor  = NULL;
    this->left = NULL;
    this->right = NULL;
    this->checarIgual = checarIgual;
    this->direcoes[0] = direcoesX;
    this->direcoes[1] = direcoesY;

    return (Kdtree) this;
}

// Inserts a new node and returns root of modified tree
// The parameter depth is used to decide axis of comparison
Kdtree insert_arvore_recursive(Kdtree _tree,Item item,unsigned depth)//! FUNCIONANDO!!!
{
    struct Kdtree* tree = (struct Kdtree *)_tree;
    if(tree->valor == NULL){
        tree->valor = item; //provavelmente corrigiu
        return tree;
    }

    unsigned currentDim = depth % 2; //para duas dimensões

    if(tree->direcoes[currentDim](item,tree->valor) < 0){
        if (!tree->left)
            tree->left = create_arvore(tree->checarIgual,tree->direcoes[0],tree->direcoes[1]);
        insert_arvore_recursive((Kdtree)tree->left,item,depth + 1);
        return tree->left;
    }
    else{
        if (!tree->right)
            tree->right = create_arvore(tree->checarIgual,tree->direcoes[0],tree->direcoes[1]);
        insert_arvore_recursive((Kdtree)tree->right,item,depth + 1);
        return tree->right;
    }

    return tree;
}
// Function to insert a new point with given point in
// KD Tree and return new root. It mainly uses above recursive
// function "insertRec()"
Kdtree insert_arvore(Kdtree kdtree, Item item)
{
    return insert_arvore_recursive(kdtree, item, 0);
}


// Searches a Item represented by "item" in the K D tree.
// The parameter depth is used to determine current axis.
Kdtree search_arvore_recursive(Kdtree _tree, Item item, unsigned depth)//! FUNCIONANDO!!!
{
    struct Kdtree* tree = (struct Kdtree *)_tree;
    // Base cases
    if (tree == NULL)
        return NULL;
    if (tree->checarIgual(tree->valor,item))
        return tree;
 
    // Current dimension is computed using current depth and total
    // dimensions (k)
    unsigned currentDim = depth % 2;
 
    // Compare point with tree with respect to cd (Current dimension)
    
    if (tree->direcoes[currentDim](item,tree->valor) < 0)
        return search_arvore_recursive(tree->left, item, depth + 1);
    return search_arvore_recursive(tree->right, item, depth + 1);
}

Kdtree search_arvore(Kdtree tree, Item item)
{
    return search_arvore_recursive(tree,item,0);
}

Kdtree find_min_recursive(Kdtree _tree, int d,unsigned depth)
{
    struct Kdtree* tree = (struct Kdtree *)_tree;
    if(tree == NULL)
    {
        return NULL;
    }
    unsigned currentDim = depth % 2;

    if(currentDim == d)
    {
        if(tree->left == NULL)
            return tree;
        return find_min_recursive(tree->left, d, depth+1);
    }
    return min_kdtree( tree, find_min_recursive(tree->left,d,depth + 1) ,find_min_recursive(tree->right, d, depth + 1),d);
}

Kdtree find_min(Kdtree tree, int d)//! FUNCIONANDO!!!
{
    return find_min_recursive(tree, d, 0);
}

Kdtree min_kdtree(Kdtree _a,Kdtree _b,Kdtree _c, int depth){
    struct Kdtree* a = (struct Kdtree *)_a;
    struct Kdtree* b = (struct Kdtree *) _b;
    struct Kdtree* c = (struct Kdtree *) _c;

    struct Kdtree* min = a;
    which_side funcao = a->direcoes[depth];
    if(b && (funcao(b->valor,min->valor) < 0) )
    {
        min = b;
    }
    if(c && (funcao(c->valor,min->valor) < 0) )
        min = c;
    return min;
}

int min_between_3(int x, int y, int z)
{
    return min(x, min(y, z));
}

// void copyItem(Item item1, Item item2)
// {
//    for (int i=0; i<2; i++)
//        item1->point[i] = item2->point[i];
// }

Kdtree delete_node_recursive(Kdtree _tree, Item item, int depth)//! FUNCIONANDO!!!
{
    
    struct Kdtree* tree =(struct Kdtree *)_tree;
    if(tree == NULL)
    {
        return NULL;
    }

    int currentDim = depth % 2;

    if(tree->checarIgual(tree->valor,item))//if node to be deleted is present at root
    {
        if(tree->right != NULL)
        {
            struct Kdtree* min = find_min(tree->right,currentDim);
     
            // copyItem(tree->valor,min->value);
            tree->valor = min->valor;

            tree->right = delete_node_recursive(tree->right, min->valor, depth + 1);
        }else if(tree->left != NULL)
        {
            struct Kdtree* max = find_max(tree->left,currentDim);
     
            // copyItem(tree->valor,min->value);
            tree->valor = max->valor;

            tree->left = delete_node_recursive(tree->left, max->valor, depth + 1);
        }else//if node to be deleted is node
        {
            free(tree);
            return NULL;
        }
        return tree;
    }

    if( tree->direcoes[currentDim](item,tree->valor) == -1)
    {
        tree->left = delete_node_recursive(tree->left, item, depth+1);
    }
    else
    {
        tree->right = delete_node_recursive(tree->right, item, depth+1);
    }
    return tree;
}

Kdtree delete_node(Kdtree tree,Item item)//! FUNCIONANDO!!!
{
    return delete_node_recursive(tree,item,0);
}

Item get_item_arvore(Kdtree _tree){//! FUNCIONANDO!!!
    struct Kdtree* tree = (struct Kdtree*)_tree;
    if(tree->valor != NULL){
        return tree->valor;
    }
    return NULL;
}

void set_item_arvore(Kdtree _tree,Item item){
    struct Kdtree* tree = (struct Kdtree*)_tree;
    tree->valor = item;
}

void print_inside(Kdtree _tree, void (*print)(void* item)){//! FUNCIONANDO!!!
    struct Kdtree* tree = (struct Kdtree*)_tree;
    if(tree->left){
        print_inside(tree->left,print);
    }
    print(tree->valor);
    if(tree->right){
        print_inside(tree->right,print);
    }
}

Kdtree find_max_recursive(Kdtree _tree, int d,unsigned depth)
{
    struct Kdtree* tree = (struct Kdtree *)_tree;
    if(tree == NULL)
    {
        return NULL;
    }
    unsigned currentDim = depth % 2;

    if(currentDim == d)
    {
        if(tree->right == NULL)
            return tree;
        return find_max_recursive(tree->right, d, depth+1);
    }
    return max_kdtree( tree, find_max_recursive(tree->left,d,depth + 1) ,find_max_recursive(tree->right, d, depth + 1),d);
}

Kdtree find_max(Kdtree tree, int d)//! FUNCIONANDO!!!
{
    return find_max_recursive(tree, d, 0);
}

Kdtree max_kdtree(Kdtree _a,Kdtree _b,Kdtree _c, int depth){
    struct Kdtree* a = (struct Kdtree *)_a;
    struct Kdtree* b = (struct Kdtree *) _b;
    struct Kdtree* c = (struct Kdtree *) _c;

    struct Kdtree* max = a;
    which_side funcao = a->direcoes[depth];
    if(b && (funcao(b->valor,max->valor) > 0) )
    {
        max = b;
    }
    if(c && (funcao(c->valor,max->valor) > 0) )
        max = c;
    return max;
}

void convert_tree_to_list_recursive(Kdtree _tree,Lista treeList){
    struct Kdtree* tree= _tree;
    if(tree->left){
    //printf("oi\n");
        convert_tree_to_list_recursive(tree->left,treeList);
    }
    insert_lista(treeList,tree->valor);
    if(tree->right){
        convert_tree_to_list_recursive(tree->right,treeList);
    }
}

Lista convert_tree_to_list(Kdtree _tree){
    struct Kdtree* tree= _tree;
    Lista treeList = create_lista();
    convert_tree_to_list_recursive(tree,treeList);
    return treeList;
    
}

void percorrer_tree_recursive(Kdtree _tree, function_during_tree executar,va_list _parameters){
    struct Kdtree* tree = _tree;
    va_list parameters;
    va_copy(parameters,_parameters);

    Item item;
    if(tree->left){
        percorrer_tree_recursive(tree->left,executar,_parameters);
    }
    item = (Item)get_item_arvore(tree);

    executar(item,parameters);
    va_end(parameters);

    if(tree->right){
        percorrer_tree_recursive(tree->right,executar,_parameters);
    }
}

void percorrer_tree(Kdtree _tree,function_during_tree executar,...){
    va_list parameters;
    va_start(parameters,executar);

    percorrer_tree_recursive(_tree,executar,parameters);
    
    va_end(parameters);
}
//bool are_values_equal(Item item1,Item item2)
// {
//     if(item1 == item2)
//         return true;
//     else
//         return false;
// }