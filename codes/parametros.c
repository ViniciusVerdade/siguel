#include "../headers/includes.h"
#include "enum.h"

Arguments *argumentos(int argc,const char* argv[] )
{
    int i,j,k;/* contadores*/
    int val1=0,val2=0;/*variavel*/
    Arguments *argumentos = newArguments();
    
    /*todo IMPLEMENTAR A CORREÇÃO PARA CASO SEJA INSERIDO OU NÃO "/" NO FINAL DO PATH E DO DIRETÓRIO*/
    for(i=1 ; i < argc ; i++)
    {
        if( strcmp(argv[i] ,"-f") == 0) /*? TRABALHANDO O NOME DO ARQUIVO*/
        {
            j = i+1;

            argumentos->NameGeoFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            
            strcpy(argumentos->NameGeoFile,argv[j]);
        }

        if( strcmp(argv[i] ,"-o") == 0)/*? TRABALHANDO O DIRETORIO EM QUE SERÁ CRIADO OS ARQUIVOS*/
        {
            j = i+1;

            argumentos->directory = (char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->directory, argv[j]);
        }
        
        if( strcmp(argv[i],"-e") == 0) 
        {
            j = i+1;
            val1 = 1;

            argumentos->path = (char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->path, argv[j]);
            k = strlen(argumentos->path)-1;
        }

        if( strcmp(argv[i],"-q") == 0)
        {
            j = i+1;
            val2 = 2;

            argumentos->NameQryFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->NameQryFile, argv[j]);
        }

        if( strcmp(argv[i],"-ec") == 0)
        {
            j = i+1;
            val2 = 2;

            argumentos->NameEcFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->NameEcFile, argv[j]);
        }

        if( strcmp(argv[i],"-pm") == 0)
        {
            j = i+1;
            val2 = 2;

            argumentos->NamePmFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->NamePmFile, argv[j]);
        }
    }
    
//
    if(argumentos->NameGeoFile[0] == '/')
    {
        shiftLeft(argumentos->NameGeoFile,1) ;
    }
    
    if(argumentos->NameQryFile)    
    {
        if(argumentos->NameQryFile[0] == '/')
        {
            shiftLeft(argumentos->NameQryFile,1) ;
        }
    }
    if(argumentos->NamePmFile)    
    {
        if(argumentos->NamePmFile[0] == '/')
        {
            shiftLeft(argumentos->NamePmFile,1) ;
        }
    }

    if(argumentos->NameEcFile)    
    {
        if(argumentos->NameEcFile[0] == '/')
        {
            shiftLeft(argumentos->NameEcFile,1) ;
        }
    }

    if(argumentos->path) 
    {
        if(argumentos->path[k] != '/')
        {
            strcat(argumentos->path,"/");
        }
    }
    i = strlen(argumentos->directory)-1;
    if(argumentos->directory[i] != '/')
    {
        strcat(argumentos->directory,"/");
    }
//
    if(val1 == 1) //! CASO tenha a chamada -e que indica o caminho do arquivo
    {
        argumentos->pathOfNameGeoFile = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->NameGeoFile)+1,sizeof(char));
        strcpy(argumentos->pathOfNameGeoFile,argumentos->path);
        strcat(argumentos->pathOfNameGeoFile,argumentos->NameGeoFile);
        if(argumentos->NameQryFile){
            argumentos->pathOfNameQryFile = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->NameQryFile)+1,sizeof(char));
            strcpy(argumentos->pathOfNameQryFile,argumentos->path);
            strcat(argumentos->pathOfNameQryFile,argumentos->NameQryFile);
        }
        if(argumentos->NamePmFile){
            argumentos->pathOfNamePmFile = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->NamePmFile)+1,sizeof(char));
            strcpy(argumentos->pathOfNamePmFile,argumentos->path);
            strcat(argumentos->pathOfNamePmFile,argumentos->NamePmFile);
        }
        if(argumentos->NameEcFile){
            argumentos->pathOfNameEcFile = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->NameEcFile)+1,sizeof(char));
            strcpy(argumentos->pathOfNameEcFile,argumentos->path);
            strcat(argumentos->pathOfNameEcFile,argumentos->NameEcFile);
        }
    }
    else //! CASO o caminho do arquivo esteja definido apenas no nome do arquivo
    {
        argumentos->pathOfNameGeoFile = (char*)calloc(strlen(argumentos->NameGeoFile)+1,sizeof(char));
        strcat(argumentos->pathOfNameGeoFile,argumentos->NameGeoFile);
        if(argumentos->NameQryFile){
            argumentos->pathOfNameQryFile = (char*)calloc(strlen(argumentos->NameQryFile)+1,sizeof(char));
            strcat(argumentos->pathOfNameQryFile,argumentos->NameQryFile);
        }
        if(argumentos->NamePmFile){
            argumentos->pathOfNamePmFile = (char*)calloc(strlen(argumentos->NamePmFile)+1,sizeof(char));
            strcat(argumentos->pathOfNamePmFile,argumentos->NamePmFile);
        }
        if(argumentos->NameEcFile){
            argumentos->pathOfNameEcFile = (char*)calloc(strlen(argumentos->NameEcFile)+1,sizeof(char));
            strcat(argumentos->pathOfNameEcFile,argumentos->NameEcFile);
        }
    }
    // if(val2 == 2) 
    // {
    //     if(val1 == 1)
    //     {
    //         argumentos->pathOfNameQryFile = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->NameQryFile)+1,sizeof(char));
    //         strcpy(argumentos->pathOfNameQryFile,argumentos->path);
    //         strcat(argumentos->pathOfNameQryFile,argumentos->NameQryFile);
    //     }
    //     else
    //     {
    //         argumentos->pathOfNameQryFile = (char*)calloc(strlen(argumentos->NameQryFile)+1,sizeof(char));
    //         strcat(argumentos->pathOfNameQryFile,argumentos->NameQryFile);
    //     }
    // }

    nome_arquivo(TXT,argumentos);
    nome_arquivo(SVG,argumentos);
    return argumentos;
}

